import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class HRCrossWords {

	static Scanner sc = new Scanner(System.in);
	static String[][] board = new String[10][10];
	static ArrayList<String> words;// = new ArrayList<String>();
	static ArrayList<String> tempWords = new ArrayList<String>();
	static int[] verticalFields = new int[10];
	static int[] horizontalFields = new int[10];
	static int[][] intersections = new int[20][2];

	public static void main(String[] args) {

		readBoard();
		readWords();
		sc.close();

	}

	static void readBoard() {
		for (int i = 0; i < 3; i++) {
			String input = sc.nextLine();
			board[i] = input.split("");
		}
		System.out.println(Arrays.deepToString(board));
	}

	static void readWords() {
		String temp = sc.nextLine();
		words = new ArrayList<String>(Arrays.asList(temp.split(";")));
		// Collections.addAll(words, temp.split(";")); //if Array list
		// instantiates in class body
		System.out.println(words);

	}

	static void insertWordHorizont() {

	}

	static void insertWordVertic() {

	}

}
