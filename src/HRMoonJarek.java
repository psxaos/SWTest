import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-06.
 */

class Graph {
    public List<Integer>[] adj;
    public boolean visited[];

    public Graph(int n) {
        this.adj = new ArrayList[n];
        for (int i = 0; i < n; ++i) {
            this.adj[i] = new ArrayList<>();
        }
    }

    public void add_directed_edge(int u, int v) {
        adj[u].add(v);
    }

    public void add_undirected_edge(int u, int v) {
        adj[u].add(v);
        adj[v].add(u);
    }

    int dfs_visit(int u) {
        visited[u] = true;
        int cc = 1;
        for (int i = 0; i < adj[u].size(); ++i) {
            int v = adj[u].get(i);
            if (!visited[v]) {
                cc += dfs_visit(v);
            }
        }
        return cc;
    }

    long dfs() {
        visited = new boolean[adj.length];

        long total = 0;

        for (int u = 0; u < adj.length; ++u) {
            visited[u] = false;
        }

        for (int u = 0; u < adj.length; ++u) {
            if (!visited[u]) {
                int cc = dfs_visit(u);
                total += ((long) cc * (long) (cc - 1)) / 2;
            }
        }
        return total;
    }
}

public class HRMoonJarek {


    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int noAstronauts = sc.nextInt();
        int noPairs = sc.nextInt();


        Graph g = new Graph(noAstronauts);
        for (int i = 0; i < noPairs; i++) {
            int firstId = sc.nextInt();
            int secondId = sc.nextInt();

            g.add_undirected_edge(firstId, secondId);
        }
        sc.close();

        long result = (long) ((noAstronauts) * (noAstronauts - 1)) / 2; //total number of pairs of astronauts
        result -= g.dfs();

        System.out.println(result);
    }
}
