import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-08.
 */
public class HRIceCreamParlor {
    // static File file = new File("icecreamParlorTest1.txt");

    static Scanner sc = new Scanner(System.in);
//    static {
//        try {
//            sc = new Scanner(file);
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//    }

    public static void main(String[] args) {


        int noCases = sc.nextInt();
        int amount, noFlavours;

        for (int i = 0; i < noCases; i++) {
            amount = sc.nextInt();
            noFlavours = sc.nextInt();


            System.out.print(solution(amount, noFlavours));
            System.out.println("");
        }


        sc.close();


    }

    private static String solution(int amount, int noFlavours) {


        Flavour[] flavours = readFlavours(noFlavours);
        //zapisujemy indexy smaków

        Arrays.sort(flavours);

        int smallerFlavourIndex, greaterFlavourIndex; //to będą indeksy znalezionych smaków


        int left = 0, right = noFlavours - 1;

        while (left < right) {
            if (flavours[left].getPrice() + flavours[right].getPrice() == amount) {
                //trafiliśmy na smaki
                smallerFlavourIndex = flavours[left].getIndex();
                greaterFlavourIndex = flavours[right].getIndex();
                if (smallerFlavourIndex < greaterFlavourIndex) {
                    return smallerFlavourIndex + " " + greaterFlavourIndex;
                } else {
                    return greaterFlavourIndex + " " + smallerFlavourIndex;
                }


            } else if (flavours[left].getPrice() + flavours[right].getPrice() > amount) {
                //za dużo, zmniejszamy prawą
                right--;
            } else if (flavours[left].getPrice() + flavours[right].getPrice() < amount) {
                //za mało, zwiększamy lewą
                left++;
            }
        }


        return "no solutions";

    }


    private static Flavour[] readFlavours(int noFlavours) {
        Flavour[] flavours = new Flavour[noFlavours];
        int temp = 0;
        for (int i = 0; i < noFlavours; i++) {
            temp = sc.nextInt();
            flavours[i] = new Flavour();
            flavours[i].setPrice(temp);
            flavours[i].setIndex(i + 1);
        }

        return flavours;
    }


}


class Flavour implements Comparable<Flavour> {
    int index;
    int price;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public int compareTo(Flavour o) {
        return this.getPrice() - o.getPrice();
    }
}