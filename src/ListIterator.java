
public class ListIterator {

	LinkForIterator current;
	LinkForIterator previous;
	ListForIterator myList;

	public ListIterator(ListForIterator x) {
		myList = x;
		reset();
	}

	public void reset() {
		current = myList.getFirst();
		previous = null;
	}

	public void nextLink() {
		previous = current;
		current = current.next;
	}

	public LinkForIterator getCurrent() {
		return current;
	}

	public boolean atEnd() {
		return (current.next == null);
	}

	public void insertAfter(double x) { // insert after current link
		LinkForIterator newLink = new LinkForIterator(x);

		if (myList.isEmpty()) { // we are at the begining
			myList.setFirst(newLink);
			current = newLink;
		} else {// not empty
			newLink.next = current.next;
			current.next = newLink;
			nextLink();
		
		}
	}

	public void insertBefore(double x) {// insert before current
		LinkForIterator newLink = new LinkForIterator(x);

		if (previous == null) {// empty list or insert at beginning
			newLink.next = myList.getFirst();
			myList.setFirst(newLink);
			reset();
		} else {// not at beginning
			newLink.next = previous.next;
			previous.next = newLink;
			current = newLink;
		}
	}

	public double deleteCurrent() {
		double value = current.data;
		if (previous == null) {// beginning of the list
			myList.setFirst(current.next);
			reset();
		} else {// not the beginning of list
			previous.next = current.next;
			if (atEnd()) {
				reset();
			} else {
				current = current.next;
			}
		}
		return value;
	}

}
