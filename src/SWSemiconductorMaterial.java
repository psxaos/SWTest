import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class SWSemiconductorMaterial {
	// max no of row masks = 89, max 10 rows,
	// data
	static int row, col;
	// mask
	static Mask[] maskArray;
	static int maskArrayLastInsPtr = 0;
	static Mask m;

	static int[][] wafer;

	public static void main(String[] args) {
		// File file = new File("input01.dat");
		// Scanner sc = null;
		// try {
		// sc = new Scanner(file);
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		Scanner sc = new Scanner(System.in);

		int noTests = sc.nextInt();

		for (int t = 0; t < noTests; t++) {
			row = sc.nextInt();
			col = sc.nextInt();
			wafer = new int[row][col];

			// read wafer data fom file
			// for (int c = 0; c < col; c++) {
			// for (int r = 0; r < row; r++) {
			// wafer[r][c] = sc.nextInt();

			// }
			// }
			// init size of possible masks array
			int maxMasks = noOfPossibleArrangements(col);
			maskArray = new Mask[maxMasks];
			// generate all possible masks
			// insert 00000 mask into mask Array on position 0

			m = new Mask(col);
			maskArrayLastInsPtr++;
			generate(m, 0);

			
			//////////////////////////////////////////////////////////////////////////////////////
			
			
			System.out.println("# " + noTests + "result");
		}

		if (sc != null) {
			sc.close();
		}
	}

	// index of wafer's row
	static Mask generateDirtMask(int index) {
		Mask convertedMask = new Mask(col);

		for (int i = 0; i < col - 1; i++) {
			if (wafer[index][i] == 1 || wafer[index][i + 1] == 1 || wafer[index + 1][i] == 1
					|| wafer[index + 1][i + 1] == 1) {
				convertedMask.mask[i] = 1;
			}
		}
		return convertedMask;
	}

	// couts how many row mask arrangements is possible
	static int noOfPossibleArrangements(int waferColumns) {
		// no of all possible arrangement in the MASK representation is
		// Fibonacci number of wafer cols-1
		// if row starts from 0 then we are left with problem of n-1 size
		// if row starts from 1, the next position must be 0, so we are left
		// with problem of n-2 size
		// total sum is f(n-1) + f(n-2)
		int result = 1;
		int[] fibo = { 1, 1 }; // 1st and second Fibonacci numbers
		for (int f = 1; f <= waferColumns; f++) {
			int current = fibo[0] + fibo[1];
			fibo[0] = fibo[1];
			fibo[1] = current;
		}
		return fibo[1];
	}

	// generate possible 1xn masks and store them in maskArray
	static void generate(Mask m, int index) {
		m = new Mask(m);
		if (index == 9) {
			addToMaskArray(m);
		} else {
			m.mask[index] = 1;
			if (index < 8) {
				m.mask[index + 1] = 0;
				generate(m, index + 2);
			} else {
				generate(m, index + 1);
			}

			m.mask[index] = 0;
			generate(m, index + 1);
		}

	}

	static void addToMaskArray(Mask m) {
		maskArray[maskArrayLastInsPtr] = m;
		maskArrayLastInsPtr++;
	}

}

// represents the possible row masks of 2x2 areas in a 2 cells row of the wafer
// converted to 1 row 0s & 1s
// 0000000000 (n)
// 0000000000 (n)
// converts to:
// 000000000 (n-1)
class Mask {
	int width;
	int[] mask;
	int pointer = 0;

	// width = no of cols of the wafer
	Mask(int noOfWaferColumns) {
		width = noOfWaferColumns - 1;
		mask = new int[width];
	}

	Mask(Mask parent) {
		this(parent.width + 1);
		this.pointer = parent.pointer;
		for (int i = 0; i < parent.width; i++) {
			this.mask[i] = parent.mask[i];
		}
	}

	void addSubmask0() {
		mask[pointer] = 0;
		pointer++;
	}

	void addSubmask1() {
		mask[pointer] = 1;
		mask[pointer + 1] = 0;
		pointer = pointer + 2;
	}

	boolean isFull() {
		if ((mask[width - 2] == 1 && pointer >= width - 2) || (mask[width - 2] == 0 && pointer > width - 1)) {
			return true;
		}
		return false;
	}

	boolean checkMaskCollision(Mask m) {
		for (int i = 1; i < width - 1; i++) {
			if (this.mask[i] == 1 && (m.mask[i] == 1 || m.mask[i + 1] == 1))
				return true;
		}
		return false;

	}

	int countSquares() {
		int count = 0;
		for (int i = 0; i < width; i++) {
			if (this.mask[i] == 1) {
				count++;
			}
		}
		return count;
	}
}
