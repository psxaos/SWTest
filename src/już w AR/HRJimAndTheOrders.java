import java.util.Scanner;

public class HRJimAndTheOrders {

	static BurgerOrder[] arr;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int noFans = sc.nextInt();
		arr = new BurgerOrder[noFans];
		// read data
		for (int i = 0; i < noFans; i++) {
			arr[i] = new BurgerOrder(sc.nextInt(), sc.nextInt(), i + 1);
		}
		quicksort(0, noFans - 1);

		// output
		for (int i = 0; i < noFans; i++) {
			System.out.print(arr[i].no + " ");
		}
	}

	private static void quicksort(int left, int right) {
		int leftPtr = left, rightPtr = right;
		BurgerOrder pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (compare(arr[leftPtr], pivot) < 0) {
				leftPtr++;
			}
			while (compare(arr[rightPtr],pivot) > 0) {
				rightPtr--;
			}

			if (leftPtr <= rightPtr) {
				swap(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}

		// recursion
		if (left < rightPtr) {
			quicksort(left, rightPtr);
		}
		if (right > leftPtr) {
			quicksort(leftPtr, right);
		}

	}

	
	// if object is greater than pivot return +1, else -1
	private static int compare(BurgerOrder b1, BurgerOrder b2) {
		if (b1.readyTime == b2.readyTime) {
			return (b1.no - b2.no); 
		}
		return (b1.readyTime - b2.readyTime);
	}

	static void swap(int l, int p) {
		BurgerOrder temp = arr[l];
		arr[l] = arr[p];
		arr[p] = temp;
	}

}

class BurgerOrder {
	int no = 0;
	int orderTime = 0;
	int timeToProcess = 0;
	int readyTime;

	BurgerOrder() {
	}

	BurgerOrder(int n, int t, int no) {
		this.orderTime = n;
		this.timeToProcess = t;
		this.no = no;
		calculateReadyTime(n, t);
	}

	void calculateReadyTime(int ot, int ttp) {
		readyTime = ot + ttp;
	}
}
