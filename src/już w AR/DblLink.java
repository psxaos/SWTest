
public class DblLink {
	public double data;
	public DblLink next;
	public DblLink previous;

	public DblLink(double x) {
		next = null;
		previous = null;
		data = x;
	}
	
	public void display(){
		System.out.print(data+" ");
	}

}
