import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-29.
 */
public class HRPairs {
    static int diff;
    static int no;
    static int arr[];
    static int result = 0;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        no = sc.nextInt();
        diff = sc.nextInt();
        arr = new int[no];

        for (int i = 0; i < no; i++) {
            arr[i] = sc.nextInt();
        }
        sc.close();
        //sorting
        quicksort(0, no - 1);

        //find pairs
        for (int i = 0; i < no - 1; i++) {
            int j = 1;

            while ((arr[i + j] - arr[i]) <= diff) {
                if ((arr[i + j] - arr[i]) == diff) {
                    result++;
                }
                j++;
                if (i + j >= no ) {
                    break;
                }
            }

        }


        System.out.println("" + result);
    }

    static void quicksort(int lo, int hi) {
        int i = lo;
        int j = hi;
        int pivot = arr[lo + (hi - lo) / 2];

        while (i <= j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            if (i <= j) {
                swap(i, j);
                i++;
                j--;
            }
        }
        //recursion
        if (j > lo)
            quicksort(lo, j);
        if (i < hi)
            quicksort(i, hi);

    }

    static void swap(int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


}

