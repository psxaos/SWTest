import java.util.Scanner;

public class HRMaximumPerimeterTriangle {

	static int[] arr;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		arr = new int[n];

		// read data
		while (n > 0) {
			arr[n - 1] = sc.nextInt();
			n--;
		}
		sc.close();
		// sort
		quicksort(0, arr.length - 1);

		for (int i = 0; (i + 2) < arr.length; i++) {

			if (arr[i] < (arr[i + 1] + arr[i + 2])) {
				System.out.println(arr[i + 2] + " " + arr[i + 1] + " " + arr[i]);
				return;
			}
		}
		System.out.println("-1");

	}

	static void quicksort(int lo, int hi) {
		int loPtr = lo;
		int hiPtr = hi;
		int pivot = arr[lo + (hi - lo) / 2];

		while (loPtr <= hiPtr) {

			while (arr[loPtr] > pivot) {
				loPtr++;
			}

			while (arr[hiPtr] < pivot) {
				hiPtr--;
			}

			if (loPtr <= hiPtr) {
				swap(loPtr, hiPtr);
				loPtr++;
				hiPtr--;
			}
		}

		// recursion
		if (lo < hiPtr) {
			quicksort(lo, hiPtr);
		}
		if (hi > loPtr) {
			quicksort(loPtr, hi);
		}

	}

	static void swap(int a, int b) {
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

}
