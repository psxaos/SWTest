import java.util.Scanner;

public class HRMarcCakewalk {
	  public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int[] calories = new int[n];
	        for(int calories_i=0; calories_i < n; calories_i++){
	            calories[calories_i] = in.nextInt();
	        }
	        
	        //1. sortujemy dwrotnie
	        quickSort(calories, 0, calories.length-1);
	       
	        //2. liczymy wynik
	        long result = 0;
	        for(int i = 0; i < calories.length; i++){
	        	result = (long) (result + calories[i]*Math.pow(2, i));
	        }
	        System.out.println(result);
	    }

	private static void quickSort(int[] calories, int left, int right) {
		//reverseQS
		int leftPtr = left, rightPtr = right;
		int pivot = calories[left + (right-left)/2];
		
		while(leftPtr <= rightPtr){
			while(calories[leftPtr] > pivot){
				leftPtr++;
			}
			
			while(calories[rightPtr] < pivot){
				rightPtr--;
			}
			
			if(leftPtr <= rightPtr){
				swap(calories, leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}
		
		//recursion
		if(left < rightPtr){
			quickSort(calories, left, rightPtr);
		}
		if(right > leftPtr){
			quickSort(calories, leftPtr, right);
		}	
		
	}

	private static void swap(int[] arr, int leftPtr, int rightPtr) {
		int temp = arr[leftPtr];
		arr[leftPtr] = arr[rightPtr];
		arr[rightPtr] = temp;
	}
	  
	  
	  
	}
