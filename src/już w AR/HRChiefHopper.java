import java.util.Scanner;

public class HRChiefHopper {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int[] arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		sc.close();

		double eOnPreviousBuilding; // to jump and not to go
		// below zero
		// E
		double energy = 0; // energy calculated for actual position, in the
						// end it will be the solution

		for (int j = arr.length - 1; j >= 0; j--) {// iterate backwards
			double temp = (energy + arr[j]) / 2;
			eOnPreviousBuilding = Math.round(temp);// energy at
													// current
													// position
			energy = eOnPreviousBuilding; // energy on previous(next in loop)
											// building should have that value
											// so we can jump to the current
											// building not going below 0
		}

		System.out.println((int) energy);
	}
}
