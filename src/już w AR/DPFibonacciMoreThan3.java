
public class DPFibonacciMoreThan3 {
//works only for input more greter or equal 2
	public static void main(String[] args){
		
		System.out.println(noOfPossibleArrangements(2));
		
		
	}

	static int noOfPossibleArrangements(int waferColumns) {

		int result = 1;
		int[] fibo = { 1, 1 }; // 1st and second Fibonacci numbers
		for (int f = 1; f <= waferColumns; f++) {
			int current = fibo[0] + fibo[1];
			fibo[0] = fibo[1];
			fibo[1] = current;
		}
		return fibo[1];
	}
}
