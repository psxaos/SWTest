import java.util.Scanner;

public class HRLuckBalance {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt(); // no of contests
		int k = sc.nextInt(); // no of important contests she can loose

		int[][] contests = new int[n][2];
		int idx = 0;

		while (idx < n) {
			int luck = sc.nextInt();
			int importance = sc.nextInt();

			contests[idx][0] = luck;
			contests[idx][1] = importance;
			idx++;}
		
		// sort array
		quicksort(contests, 0, contests.length - 1);

		// iterate and count Lena's rank removing highest important contests and
		// unimportant ones
		idx = n-1;
		int lostImp = 0;
		int result = 0;
		
		while (idx >= 0) {
			if (contests[idx][1] == 1) {
				if (lostImp < k) {
					lostImp++;
					result = result + contests[idx][0];
				} else {
					result = result - contests[idx][0];
				}
			} else {
				result = result + contests[idx][0];
			}
			idx--;
		}
		sc.close();
		System.out.println(result);
	}

	static void quicksort(int[][] arr, int lo, int hi) {
		int loPtr = lo;
		int hiPtr = hi;
		int pivot = arr[lo + (hi - lo) / 2][0];

		while (loPtr <= hiPtr) {
			while (arr[loPtr][0] < pivot) {
				loPtr++;
			}

			while (arr[hiPtr][0] > pivot) {
				hiPtr--;
			}

			if (loPtr <= hiPtr) {
				swap(arr, loPtr, hiPtr);
				loPtr++;
				hiPtr--;
			}
		}

		if (lo < hiPtr) {
			quicksort(arr, lo, hiPtr);
		}
		if (hi > loPtr) {
			quicksort(arr, loPtr, hi);
		}

	}

	static void swap(int[][] arr, int a, int b) {
		int temp0 = arr[a][0];
		int temp1 = arr[a][1];

		arr[a][0] = arr[b][0];
		arr[b][0] = temp0;

		arr[a][1] = arr[b][1];
		arr[b][1] = temp1;
	}
}
