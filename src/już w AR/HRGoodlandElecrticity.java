import java.util.Scanner;

public class HRGoodlandElecrticity {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int d = sc.nextInt();
		int[] arr = new int[n];
		d = d - 1;
		int j = 0;
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();

		} // for EO data input
		int i = 0;
		int counter = count(arr, d);

		System.out.println(counter);
	}

	static int count(int arr[], int d) {
		int i = 0;
		int n = arr.length;
		int counter = 0;
		int mem = 0;

		while (i <= n) {// iterate the whole array
			int j = i - d;

			int lastInRange = 0;
			boolean found = false; // flag if in tower range we found the tower
			while (j < i + d) { // iterate thorough the range of the tower
				if (j < 0) { // left bordercase
					j++;
					continue;
				}

				if (arr[j] == 1) {
					mem = j; // index ostatniego s�upa w range'u
					lastInRange = j + d;
					if (found == false) {
						counter++;
					}
					found = true;
				} // if
				j++;
				if (j >= n) {// end of array
					return counter;
				}
			} // while
			if (found != true) { // no tower in range
				return -1;
			}

			i = mem + d + 1;

		}

		return counter;
	}
}
