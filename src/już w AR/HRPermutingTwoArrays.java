import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Scanner;

public class HRPermutingTwoArrays {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		for (int i = 0; i < t; i++) {
			int n = sc.nextInt();
			int k = sc.nextInt();

			int[] arrA = new int[n];
			int[] arrB = new int[n];
			// read array data
			for (int j = 0; j < n; j++) {
				arrA[j] = sc.nextInt();
			}
			for (int j = 0; j < n; j++) {
				arrB[j] = sc.nextInt();
			}
			quicksort(arrA, 0, arrA.length - 1);
			quicksort(arrB, 0, arrB.length - 1);
			
//			Arrays.sort(arrA);
//			Arrays.sort(arrB);
			boolean result = true;
			for (int j = 0; j < n; j++) {
				int temp = arrA[j] + arrB[n - 1 - j];
				if (temp < k) {
					result = false;
					break;
				}
			}
			System.out.println(result == true ? "YES" : "NO");

		}
		sc.close();
	}

	static void quicksort(int arr[], int left, int right) {
		int leftPtr = left, rightPtr = right;
		int pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot) {
				rightPtr--;
			}
			if (leftPtr <= rightPtr) {
				swap(arr, leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}
		
		
		//recurence
		if(left<rightPtr){
			quicksort(arr, left, rightPtr);
		}
		if(right>leftPtr){
			quicksort(arr, leftPtr, right);
		}

	}

	static void swap(int[] arr, int a, int b) {
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

}
