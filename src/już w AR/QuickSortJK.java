import java.util.Arrays;
import java.util.Scanner;

public class QuickSortJK {

	static int arr[];

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();
		arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}

		qsort(0, n - 1);

		for (int i = 0; i < n; i++) {
			System.out.println(arr[i] + " ");
		}
	}

	private static int partition(int left, int right) {
		int pivot = arr[left];
		int leftPtr = left;
		int rightPtr = right + 1;

		for (;;) {
			do {
				++leftPtr;
			} while ((leftPtr <= right) && (arr[leftPtr] < pivot));

			do {
				--rightPtr;
			} while (arr[rightPtr] > pivot);

			if (leftPtr >= rightPtr) {
				break;
			}
			swap(leftPtr, rightPtr);
		}
		swap(left, rightPtr);

		return rightPtr;
	}

	private static void qsort(int left, int right) {
		if (left < right) {
			int q = partition(left, right);

			qsort(left, q - 1);
			qsort(q + 1, right);
		}
	}

	private static void swap(int leftPtr, int rightPtr) {
		int temp = arr[leftPtr];
		arr[leftPtr] = arr[rightPtr];
		arr[rightPtr] = temp;
	}

}
