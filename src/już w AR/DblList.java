
public class DblList {

	DblLink first;
	DblLink last;

	public DblList() {
		first = null;
		last = null;
	}

	public boolean isEmpty() {
		return (first == null);
	}

	public void displayForward() {
		DblLink current = first;
		while (current != null) {
			current.display();
			current = current.next;
		}
	}

	public void displayBackward() {
		DblLink current = last;
		while (current != null) {
			current.display();
			current = current.previous;
		}
	}

	public void insertFirst(double x) {
		DblLink newLink = new DblLink(x);
		if (isEmpty()) {
			last = newLink;
		} else {
			first.previous = newLink;
		}
		newLink.next = first;
		first = newLink;
	}

	public void insertLast(double x) {
		DblLink newLink = new DblLink(x);
		if (isEmpty()) {
			first = newLink;
		} else {
			last.next = newLink;
			newLink.previous = last;
		}

		last = newLink;
	}

	public boolean insertAfter(double value, double x) {

		DblLink current = first;

		while (current.data != value) {

			current = current.next;
			if (current == null) {
				return false;// not found and we are at the end of the list
			}
		}
		DblLink newLink = new DblLink(x);// found; create new link
		
		if (current == last) {
			newLink.next = null;
			last = newLink;
		} else {
			newLink.next = current.next;
			current.next.previous = newLink;
		}
		newLink.previous = current;
		current.next = newLink;
		return true;
	}

	public DblLink deleteFirst() {
		DblLink temp = first;
		if (first.next == null) {// only one item
			last = null;
		} else {
			first.next.previous = null;
		}

		first = first.next;
		return temp;
	}

	public DblLink deleteLast() {
		DblLink temp = last;
		if (last.next == null) {// only one item
			first = null;
		} else {
			last.previous.next = null;

		}
		last = last.previous;
		return temp;
	}

	public DblLink deleteKey(double x) {
		DblLink current = first;
		while (current.data != x) {
			current = current.next;
			if (current == null) {
				return null;
			}
		}
		if (current == first) {// we are at beginning
			first = current.next;
		} else if (current == last) {// we are at the end
			last = current.previous;
		} else {// we are somewhere else
			current.next.previous = current.previous;
			current.previous.next = current.next;
		}
		return current;
	}

}
