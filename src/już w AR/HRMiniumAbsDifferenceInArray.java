import java.util.Scanner;

public class HRMiniumAbsDifferenceInArray {

	static int minimumAbsoluteDifference(int n, int[] arr) {
		int solution = Integer.MAX_VALUE;

		// 1. sort array
		sortArray(arr, 0, arr.length - 1);

		// 2. iterate remembering lowest difference
		for (int i = 1; i < arr.length; i++) {
			int temp = absDiff(arr[i - 1], arr[i]);

			solution = temp < solution ? temp : solution;
		}

		return solution;
	}

	public static void sortArray(int[] arr, int left, int right) {

		int leftPtr = left;
		int rightPtr = right;
		int pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot) {
				rightPtr--;
			}

			if (leftPtr <= rightPtr) {
				swap(arr, leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}
		if (left < rightPtr) {
			sortArray(arr, left, rightPtr);
		}
		if (leftPtr < right) {
			sortArray(arr, leftPtr, right);
		}
	}

	public static void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public static int absDiff(int a, int b) {
		return a - b >= 0 ? a - b : -(a - b);
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] arr = new int[n];
		for (int arr_i = 0; arr_i < n; arr_i++) {
			arr[arr_i] = in.nextInt();
		}
		int result = minimumAbsoluteDifference(n, arr);
		System.out.println(result);
		in.close();
	}
}
