import java.util.Scanner;

public class HRTemp {

	public static void main(String[] args) {

		int[] calories = { 4, 3, 6, 5, 2, 4, 7 };

		// 1. sortujemy dwrotnie
		quickSort(calories, 0, calories.length - 1);
		for (int i = 0; i < calories.length; i++) {
			System.out.print(calories[i] + " ");
		}
		// 2. liczymy wynik
		long result = 0;

	}

	private static void quickSort(int[] calories, int left, int right) {
		// reverseQS
		int leftPtr = left, rightPtr = right;
		int pivot = calories[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (calories[leftPtr] > pivot) {
				leftPtr++;
			}

			while (calories[rightPtr] < pivot) {
				rightPtr--;
			}

			if (leftPtr <= rightPtr) {
				swap(calories, leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}

		// recursion
		if (left < rightPtr) {
			quickSort(calories, left, rightPtr);
		}
		if (right > leftPtr) {
			quickSort(calories, leftPtr, right);
		}

	}

	private static void swap(int[] arr, int leftPtr, int rightPtr) {
		int temp = arr[leftPtr];
		arr[leftPtr] = arr[rightPtr];
		arr[rightPtr] = temp;
	}
}
