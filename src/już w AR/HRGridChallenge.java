import java.util.Scanner;

public class HRGridChallenge {
	static char[][] arr;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int t = sc.nextInt();
		int n;
		String result = "YES";


		// fill the matrix up
		for (int i = 0; i < t; i++) {
			// for every test case
			n = sc.nextInt();
			arr = new char[n][n];
			for (int row = 0; row < n; row++) {
				String s = sc.next();
				for (int col = 0; col < n; col++) {
					arr[row][col] = s.charAt(col);
				}
				// 1. Sort
				quicksort(row, 0, n - 1);

			}

			// 2. check if cols are sorted
			for (int l = 0; l < n; l++) {// cols
				for (int m = 1; m < n; m++) {// row
					if (arr[m - 1][l ] > arr[m][l] && result.equals("YES")) {
						result = "NO";
					}
				}

			}
			System.out.println(result);

			result = "YES";
		}

		sc.close();

	}

	private static void quicksort(int row, int left, int right) {
		int leftPtr = left, rightPtr = right;
		int pivot = arr[row][left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (arr[row][leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[row][rightPtr] > pivot) {
				rightPtr--;
			}
			if (leftPtr <= rightPtr) {
				swap(row, leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}

		}
		if (left < rightPtr) {
			quicksort(row, left, rightPtr);
		}
		if (right > leftPtr) {
			quicksort(row, leftPtr, right);
		}

	}

	static void swap(int row, int a, int b) {
		char temp = arr[row][a];
		arr[row][a] = arr[row][b];
		arr[row][b] = temp;
	}

}
