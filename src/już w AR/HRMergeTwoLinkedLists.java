
public class HRMergeTwoLinkedLists {

	Node mergeLists(Node headA, Node headB) {
		Node secondHead = null;
		Node newHead = null;
		Node newCurrent = null;
		Node secondCurrent = null;
		Node next = null;
		Node temp = null;

		// determine start point of new list
		if ((headA != null) && (headB != null) && (headA.data <= headB.data)) {
			newHead = headA;
			secondHead = headB;
		} else if ((headA != null) && (headB != null) && (headB.data <= headA.data)) {
			newHead = headB;
			secondHead = headA;
		} else if ((headA == null) && (headB != null)) {
			return headB;
		} else if ((headB == null) && (headA != null)) {
			return headA;
		} else { //both lists are empty
			return null;
		}

		newCurrent = newHead; //newList
		secondCurrent = secondHead; //secondList

		while (newCurrent.next != null) {
			if (secondCurrent != null && secondCurrent.data < newCurrent.next.data) {
				// element z drugiej listy jest mniejszy ni� kolejny na nowej
				// li�cie
				temp = secondCurrent.next;
				secondCurrent.next = newCurrent.next;
				newCurrent.next = secondCurrent;
				secondCurrent = temp;				
			}
			if (secondCurrent == null) {
				//gdy sko�czy�a si� srugaLista
				break;
			}
			newCurrent = newCurrent.next;
		} //end of iteration through newList

		
		// if there are any items left in secondList - merge at the end
		if (secondCurrent != null){
			newCurrent.next = secondCurrent;
		}
		
		return newHead;
	}

}

class Node {
	int data;
	Node next;
}