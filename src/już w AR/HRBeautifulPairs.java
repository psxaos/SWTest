import java.util.Scanner;

public class HRBeautifulPairs {

	static int[] a, b;
	static int sum = 0; // total found disjoint pairs

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		a = new int[1001]; // counter matrix - number of occurrences of
							// particular number in matrix a

		// read data a - count occurrences of particular number
		for (int i = 1; i < n + 1; i++) {
			a[sc.nextInt()]++;
		}
		// read b - decrease numbers ('cause there are pairs we want to count
		// only once)
		for (int i = 1; i < n + 1; i++) {

			int number = sc.nextInt();
			if (a[number] > 0) { // we have pair!
				a[number]--;
				sum++;
			}
		}

		sc.close();

		if (sum < n) {// we have less pairs than array size so we always can
						// change one number in b to add one pair
			System.out.println(sum + 1);
		} else {// we have matrix full of pairs (pairs no = matrix size) so
				// changing one number in b decreases no of pairs
			System.out.println(sum - 1);

		}
	}

}
