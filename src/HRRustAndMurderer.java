import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-13.
 */
public class HRRustAndMurderer {
    public static void main(String[] arg) {


        //robimy tablicę odległości 1 wymiarową z jedynkami w każdej pozycji oznaczającymi odległość od STARTU
        //do danego wierzchołka (bo graf jest rzadki, a my komplementujemy go)
        //dalej tworzymy tablicę list z sąsiedztwami
        //przeszukujemy od naszego wierzchołka BFSem i teraz
        //jeśli odwiedzamy węzeł i nie ma go na liście sąsiadów STARTu to nie wliczamy go bo to oznacza,
        //że istnieje w komplementarnym grafie krawędź bezpośrednia od STARTU a więc odległość wynosi 1 (co już jest wpisane do tablcy odległości
        //idziemy do kolejnego wierzchołka i jeśli jest na liście sąsiadó STARTU, to oznacza, że droga będzie dłuższa i należy wykonać BFS
        //na grafie komplementarnym czyli po wierzchołkach, które nie mają z nim połączenia w oryginalnym grafie
        //i rekurencja

        Scanner sc = new Scanner(System.in);
        int noTest = sc.nextInt();

        for (int t = 0; t < noTest; t++) { //for every test case
            int noCities = sc.nextInt();
            int noRoad = sc.nextInt();
            MurderGraph g = new MurderGraph(noCities);

            for (int r = 0; r < noRoad; r++) {
                //add roads to the graph
                int city1 = sc.nextInt();
                int city2 = sc.nextInt();
                g.addUndirectedRoad(city1, city2);
            }
            int start = sc.nextInt();
            g.bfs(start);
        }
    }


}


class MurderGraph {
    MurderList[] adj;
    int distance[];
    boolean[] visited;

    MurderGraph(int size) {
        adj = new MurderList[size];
        visited = new boolean[size];


        this.distance = new int[size];
        for (int i = 0; i < size; i++) {
            distance[i] = 1;
        }
    }

    void addUndirectedRoad(int city1, int city2) {
        adj[city1].insert(city2);
        adj[city2].insert(city1);
    }


    //BFS
    int[] bfs(int start) {







        return distance;
    }

}


class MurderQueue {
    int[] arr;
    int head = 0;
    int tail = -1;
    int size;

    MurderQueue(int size) {
        this.arr = new int[size];
        this.size = size;
    }

    boolean put(int value) {
        if (tail < size) {
            arr[++tail] = value;
            return true;
        } else {
            return false;
        }
    }

    int pop() {
        int temp = arr[head];
        head--;
        return temp;
    }

    boolean isEmpty() {
        return head < 0;
    }
}

class MurderList {
    MurderLink first = null;

    void insert(int cityId) {
        MurderLink newLink = new MurderLink(cityId);
        newLink.next = first;
        this.first = newLink;
    }

    boolean isEmpty() {
        return first == null;
    }
}

class MurderLink {
    MurderLink next = null;
    int cityId;

    MurderLink(int cityId) {
        this.cityId = cityId;

    }

}
