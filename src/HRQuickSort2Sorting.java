import java.util.Scanner;

public class HRQuickSort2Sorting {
	static int[] arr;
	static int size;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		size = sc.nextInt();
		if (size <= 1) {
			return;
		}
		arr = new int[size];

		for (int i = 0; i < (size); i++) {
			arr[i] = sc.nextInt();
		}

		sort(size - 1, 0, size - 1);

		// for (int i = 0; i < size; i++) {
		// System.out.print(arr[i] + " ");
		// }
		// System.out.println("");
	}

	static void sort(int pivot, int left, int right) {

		int l = left;
		int r = right;
		int p = arr[pivot];

		while (l < r) {
			while (arr[l] < p) {
				l++;
			}
			while (arr[r] > p && r > 0) {
				r--;
			}
			if (r >= l) {
				swap(l, r);
				r--;
				l++;
			}
		}

		for (int j = 0; j < size; j++) {
			System.out.print(arr[j] + " ");
		}
		System.out.println("");

		if (left < r) {
			sort(r, left, r);

		}
		if (l < right) {
			sort(right, l, right);

		}

	}

	static void swap(int a, int b) {
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}
}
