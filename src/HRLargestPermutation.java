import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class HRLargestPermutation {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt() + 1;
		int k = sc.nextInt();
		int[] arr = new int[n];

		Map<Integer, Integer> hm = new HashMap<Integer, Integer>();

		for (int i = 1; i < n; i++) {
			arr[i] = sc.nextInt();
			hm.put(arr[i], i);
		}
		sc.close();
		// find 1,2,3,4 biggest elements ant put s them in front of matrix
		// for (int i = 0; (i < n) && (k > 0); i++) {
		int i = 0;
		int m = n - 1;
		while ((m > 0) && (k > 0)) {
			int selectedValue = arr[n - m]; // value to swap picked from array
			int currentMax = m; // value we look for

			if (selectedValue != currentMax) { // if is not in its place already
				int indexOfMax = hm.get(currentMax); // index of max value from
														// hm to swap
				// swap in array
				int temp = selectedValue; // remember selected value
				arr[indexOfMax] = temp; // replace the value in arr where the
										// maxValue was with selectedVAlue
				arr[n - m] = currentMax; // put maxValue into current position
											// in
											// arr
				// update hm
				hm.remove(selectedValue); // remove old selectedValue index in
											// arr
				hm.put(selectedValue, indexOfMax); // put new selected value
													// index in arr
				k--; // we made a swap so decrease the swap counter
			}
			m--; // go to next arr position

			// //if k>n then iterate from the begining
			// if (swapNo == n - 1) {
			// k = k - swapNo;
			// swapNo = 0;
			// }
		}

		// output
		for (int j = 1; j < n; j++) {
			System.out.print(arr[j] + " ");
		}
	}

}
