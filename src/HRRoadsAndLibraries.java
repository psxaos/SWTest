import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-12.
 */
public class HRRoadsAndLibraries {

    public static void main(String[] arg) {
        Scanner sc = new Scanner(System.in);
        int queries = sc.nextInt();
        long result;

        for (int i = 0; i < queries; i++) {//query
            result = 0L;
            int cities = sc.nextInt();
            int roads = sc.nextInt();
            int clib = sc.nextInt();
            int croad = sc.nextInt();

            GraphOfCities g = new GraphOfCities(cities, clib, croad);

            for (int r = 0; r < roads; r++) {
                g.addUndirectedEdge(sc.nextInt() - 1, sc.nextInt() - 1);
            }

            if (clib <= croad) {
                //budujemy biblioteki everywhere, bez dróg
                result = (long) clib * cities;
            } else {
                //jedna biblioteka i reszta dróg łączących miasta w obrębie wyspy
                //obliczamy wielkość wysp i koszt budowy dla każdej z nich: clib+(citiesNaSyspie-1)*croad
                result = g.dfs();
            }
            System.out.println(result);
        }
        sc.close();
    }
}

class GraphOfCities {
    CityList[] adj;
    boolean[] visited;
    int noCities;
    int clib, croad;

    GraphOfCities(int noCities, int clib, int croad) {
        //init array & linked list of city connections
        this.adj = new CityList[noCities];
        for (int i = 0; i < noCities; i++) {
            adj[i] = new CityList();
        }


        this.noCities = noCities;
        this.clib = clib;
        this.croad = croad;
    }

    void addUndirectedEdge(int u, int v) {
        adj[u].insert(v);
        adj[v].insert(u);
    }

    int visit(int u) {
        visited[u] = true;
        int count = 1;

        CityLink v = adj[u].first;
        while (v != null) {
            if (!visited[v.cityNumber]) { //jeśli ma sąsiada
                count += visit(v.cityNumber);
            }
            v = v.next;
        }
        return count;
    }

    long dfs() { //sprawdzamy kolejne wyspy
        visited = new boolean[noCities];
        long total = 0;
        //init visited[] array
        for (int i = 0; i < noCities; i++) {
            visited[i] = false;
        }
        for (int u = 0; u < noCities; u++) {
            if (!visited[u]) {
                total += (visit(u) - 1) * (long) croad + clib; //jedna biblioteka i n-1 dróg
            }

        }
        return total;
    }
}

class CityLink {
    CityLink next = null;
    int cityNumber;

    CityLink(int cityNumber) {
        this.cityNumber = cityNumber;
    }

}

class CityList {
    CityLink first = null;
    int size = 0;

    void insert(int cityNumber) {
        CityLink newCity = new CityLink(cityNumber);
        newCity.next = this.first;
        this.first = newCity;
        size++;
    }

    int size() {
        return size;
    }

    boolean isEmpty() {
        return first == null;
    }
}