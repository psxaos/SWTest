

public class UnionFind {
	private int[] id; //connection table that contains connected group root number
	private int[] size; // size of tree in root node i

	public UnionFind(int n) {
		id = new int[n];
		size = new int[n];

		for (int i = 0; i < n; i++) {
			id[i] = i;
			size[i] = 1;
		}
	}

	private int root(int i) {
		while (i != id[i]) {
			id[i]=id[id[i]]; //halves the tree size by setting the root node of grandparent
			i = id[i];
		}
		return i;
	}

	public boolean connected(int p, int q) {
		return root(p) == root(q);
	}

	public void union(int p, int q) {
		int i = root(p);
		int j = root(q);
		if (i == j) //the same root - already connected
			return;
		if (size[i] < size[j]) {
			id[i] = j;
			size[j] += size[i];
		} else {
			id[j] = i;
		    size[i] += size[j];
		}
	}

}
