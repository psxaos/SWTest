
public class RecBinaryFind {

	public static void main(String[] args) {

		int maxSize = 100; // array size
		ordArray arr; // reference to array
		arr = new ordArray(maxSize); // create the array
		arr.insert(72); // insert items
		arr.insert(90);
		arr.insert(45);
		arr.insert(126);
		arr.insert(54);
		arr.insert(99);
		arr.insert(144);
		arr.insert(27);
		arr.insert(135);
		arr.insert(81);
		arr.insert(18);
		arr.insert(108);
		arr.insert(9);
		arr.insert(117);
		arr.insert(63);
		arr.insert(36);

		arr.display(); // display array
		long searchKey = 27; // search for item
		if (arr.find(searchKey) != arr.size()) {
			System.out.println("Found " + searchKey);
		} else {
			System.out.println("Can't find " + searchKey);
		}

	}

}

class ordArray {
	private long[] a;
	private int nElems;

	public ordArray(int size) {// constructor
		a = new long[size];
		nElems = 0;
	}
	
	public int find(long x){
		return recFind(x,0,nElems-1);
		
	}

	public int recFind(long value, int low, int high) {
		int mid;
		mid = (low + high) / 2;
		if (a[mid] == value) { // found
			return mid;
		} else if (low > high) { // not found
			return nElems;
		} else {
			if (value > a[mid]) { // is in upper
				return recFind(value, mid + 1, high);

			} else {// is in lower
				return recFind(value, low, mid - 1);
			}
		}

	}

	public boolean insert(long x) {
		int index;
		for (index = 0; index < nElems; index++) {
			if (a[index] > x) {
				break;
			}
		}
		if (index == nElems - 1) {// array full
			return false;
		}
		// shift and insert
		for (int i = nElems; i > index; i--) {
			a[i] = a[i-1];
		}
		a[index] = x;
		nElems++;
		return true;
	}

	public long remove(int position) {

		long temp = a[position];
		for (int i = position; i < nElems; i++) {
			a[i] = a[i + 1];
		}
		nElems--;
		return temp;

	}

	public int size() {
		return nElems;
	}

	public void display() {
		for (int i = 0; i < nElems; i++) {
			System.out.print(a[i] + " ");
			System.out.println("");
		}
	}

}