import java.util.Scanner;

public class HRConnectedCellsInAGrid {

    // HR Connected Cells in a Grid
    //1 islands
    //2 visited
    static int rows, cols;
    static int[][] arr;
    static int result;

    public static void main(String[] args) {


        int biggestRegion;

        Scanner sc = new Scanner(System.in);
        rows = sc.nextInt(); //wiersze
        cols = sc.nextInt(); //kolumny
        arr = new int[rows + 2][cols + 2];

        //border case  - 2s around the map
        for (int i = 0; i < cols + 2; i++) {//poziome wiersze - dolny i górny
            arr[0][i] = 0; //visited
            arr[rows + 1][i] = 0;
        }

        for (int i = 0; i < rows + 2; i++) {//pionowe kolumny lewa i prawa
            arr[i][0] = 0; //visited
            arr[i][cols + 1] = 0;
        }

        // filling an array
        for (int r = 1; r <= rows; r++) {
            for (int c = 1; c <= cols; c++) {
                arr[r][c] = sc.nextInt();
            }
        }
        sc.close();

        //idzie w pętli po całej mapie i nie odwiedza już odwiedzonych (zaznaczone jako 2)
        //jak znajdzie 1(wyspę) to uruchamia dfs z tego miejsca
        int result = 0;
        for (int r = 1; r <= rows; r++) {
            for (int c = 1; c <= cols; c++) {
                if (arr[r][c] == 1) {
                    int tempResult = dfs(r, c);
                    if (tempResult > result) {
                        result = tempResult;
                    }
                }
            }
        }
        System.out.println(result);

    }


    static int dfs(int r, int c) {
        int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
        int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};
        int count = 1;

        arr[r][c] = 2; //visited

        for (int i = 0; i < 8; i++) {
            int x = r + dx[i];
            int y = c + dy[i];
            if (arr[x][y] == 1) {
                count = count + dfs(x, y);
            }
        }
        return count;
    }
}

