	import java.util.Scanner;

	/**

	 * Created by r.adamek on 10/17/16.

	 */

	public class CableONMRA {

	    public static void main(String[] args) {

	        Scanner in = new Scanner(System.in);

	        int T = in.nextInt();

	        for (int test_case = 1; test_case <= T; test_case++) {

	            int N = in.nextInt();

	            Graph graph = new Graph(N + 1);

	            for (int i = 0; i < N - 1; i++) graph.connect(in.nextInt(), in.nextInt(), in.nextInt());

	            System.out.println("#" + test_case + " " + graph.solve());

	        }

	    }

	 

	    static class Graph {

	        private List[] nodes;

	        private int[][] weights;

	        private int[] values;

	        private boolean[] visited;

	        public int biggestNode;

	        public int biggestValue;

	        private int solution;

	        private boolean[][] removedEdges;

	 

	        public Graph(int N) {

	            this.nodes = new List[N];

	            for (int i = 1; i < N; i++) nodes[i] = new List();

	            this.weights = new int[N][N];

	            this.biggestValue = -1;

	            this.values = new int[N];

	            this.visited = new boolean[N];

	            this.removedEdges = new boolean[N][N];

	            this.solution = -1;

	        }

	 

	        public void reset() {

	            this.biggestValue = -1;

	            this.values = new int[values.length];

	            this.visited = new boolean[visited.length];

	        }

	 

	        public void connect(int value, int a, int b) {

	            nodes[a].add(b);

	            nodes[b].add(a);

	            weights[a][b] = value;

	            weights[b][a] = value;

	        }

	 

	        public void disconnect(int a, int b) {

	            nodes[a].remove(b);

	            nodes[b].remove(a);

	        }

	 

	        public int findBiggestRoad(int startingPoint) {

	            dfs(startingPoint, 0);

	            reset();

	            int road = dfs(biggestNode, 0);

	            reset();

	            return road;

	        }

	 

	        public int dfs(int startingNode, int value) {

	            values[startingNode] += value;

	            visited[startingNode] = true;

	            if (values[startingNode] >= biggestValue) {

	                this.biggestValue = values[startingNode];

	                this.biggestNode = startingNode;

	            }

	 

	            for (int i = 0; i <= nodes[startingNode].size(); i++) {

	                int nextNode = nodes[startingNode].get(i);

	                if (!visited[nextNode])

	                    dfs(nodes[startingNode].get(i), values[startingNode] + weights[startingNode][nextNode]);

	            }

	 

	            return biggestValue;

	        }

	 

	        public int solve() {

	            for (int i = 1; i < nodes.length; i++) {

	                for (int j = 0; j < nodes[i].size(); j++) {

	                    int k = nodes[i].get(j);

	                    if (!removedEdges[i][k]) {

	                        removedEdges[i][k] = true;

	                        removedEdges[k][i] = true;

	                        disconnect(i, k);

	                        solution = max(solution, findBiggestRoad(i) + findBiggestRoad(k) + weights[i][k]);

	                        connect(weights[i][k], i, k);

	                    }

	                }

	            }

	 

	            return solution;

	        }

	    }

	 

	    static class List {

	        private static final int DEFAULT_CAPACITY = 10;

	        private int[] array;

	        private int head;

	        private int capacity;

	 

	        public List() {

	            this(DEFAULT_CAPACITY);

	        }

	 

	        public List(int capacity) {

	            this.capacity = capacity;

	            this.array = new int[capacity];

	            this.head = -1;

	        }

	 

	        public void add(int object) {

	            checkSize(++head);

	            array[head] = object;

	        }

	 

	        private void checkSize(int nextSize) {

	            if (nextSize >= capacity) {

	                capacity += capacity;

	                int[] newArray = new int[capacity];

	                System.arraycopy(array, 0, newArray, 0, array.length);

	                array = newArray;

	            }

	        }

	 

	        public int size() {

	            return head;

	        }

	 

	        public int get(int index) {

	            return array[index];

	        }

	 

	        public void removeAt(int index) {

	            if (checkIndex(index)) {

	                System.arraycopy(array, index + 1, array, index, array.length - 1 - index);

	                head--;

	            }

	        }

	 

	        public void remove(int obj) {

	            for (int i = 0; i <= head; i++) {

	                if (obj == array[i]) removeAt(i);

	            }

	        }

	 

	        public boolean contains(int value) {

	            for (int i : array) if (i == value) return true;

	            return false;

	        }

	 

	        private boolean checkIndex(int index) {

	            return index >= 0 && index <= head;

	        }

	    }

	 

	    public static int max(int a, int b) {

	        return a > b ? a : b;

	    }

	}