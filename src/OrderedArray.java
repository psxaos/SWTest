

public class OrderedArray {
	private int[] a; // array itself
	private int nElements; // no of elements in the array

	public OrderedArray(int size) { // constructor
		a = new int[size];
		nElements = 0;
	}

	public int size() {
		return nElements;
	}

	public int find(int key) { // binary search for key in the array
		int lo = 0;
		int hi = nElements - 1;
		int mid = 0;

		while (lo <= hi) {
			mid = (lo + hi) / 2;
			if (key < a[mid])
				hi = mid - 1;
			else if (key > a[mid])
				lo = mid + 1;
			else
				return mid;
		}
		return -1;
	}

	public boolean insert(int value) {
		int index;

		for (index = 0; index < nElements; index++) {// find position to insert
														// value
			if (a[index] > value) {
				break;
			}
		}
		if (index == a.length - 1)
			return false; //array full

		for (int i = nElements; i > index; i--) {//shift up
			a[i] = a[i - 1];

		}
		a[index]=value; //insert value
		nElements++; //increment size
		return true;

	}

	public boolean delete(int value) {
		int index;
		index = find(value);
		if (index == -1) { // can't find a value
			return false;
		} else {
			for (int i = index; i < nElements; i++) {
				a[i] = a[i + 1];
			}
			nElements--;
			return true;
		}
	}

	public void display() {
		for (int i = 0; i < nElements; i++) {
			System.out.print(a[i] + " ");
			System.out.println(" ");
		}
	}
}
