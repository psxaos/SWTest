import java.util.Scanner;

public class HRPriyankaAndToys {

	static int[] toys;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int result = 1;
		int n = sc.nextInt();
		toys = new int[n];
		// read data
		for (int i = 0; i < n; i++) {
			toys[i] = sc.nextInt();
		}

		// sort then iterate counting items in range
		quicksort(0, n - 1);
		int pickedItem = 0, nextItem = 1;
		int bought = 0;
		while (pickedItem < n) {

			if (!(toys[pickedItem] <= (toys[bought] + 4))) {
				result++;
				bought = pickedItem;
			}
			pickedItem++;
		}

		// for (pickedItem = 0; pickedItem < n; pickedItem++) {
		// result++;
		// while {
		// if (toys[pickedItem + nextItem] <= (toys[pickedItem] + 4)) {
		// result++;
		// }
		//
		// }
		// }
		System.out.println(result);
	}

	static void quicksort(int left, int right) {
		int leftPtr = left;
		int rightPtr = right;
		int pivot = toys[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {

			while (toys[leftPtr] < pivot) {
				leftPtr++;
			}
			while (toys[rightPtr] > pivot) {
				rightPtr--;
			}

			if (rightPtr >= leftPtr) {
				swap(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}

		// recursion
		if (leftPtr < right) {
			quicksort(leftPtr, right);
		}
		if (rightPtr > left) {
			quicksort(left, rightPtr);
		}

	}

	static void swap(int a, int b) {
		int temp = toys[a];
		toys[a] = toys[b];
		toys[b] = temp;
	}

}
