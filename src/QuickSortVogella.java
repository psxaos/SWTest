
public class QuickSortVogella {

	private static int[] numbers = { 9,6,11,4,7 };
	static private int number;

	public static void sort(int[] values) {
		if (values == null || values.length == 0) {
			return;
		}
		numbers = values;
		number = values.length;
		quicksort(0, number - 1);
	}

	private static void quicksort(int low, int high) {
		int leftPtr = low, rightPtr = high;

		int pivot = numbers[low + (high - low) / 2];

		while (leftPtr <= rightPtr) {

			while (numbers[leftPtr] < pivot) {
				leftPtr++;
			}
			while (numbers[rightPtr] > pivot) {
				rightPtr--;
			}
			if (leftPtr <= rightPtr) {
				exchange(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}
		// recursion
		if (low < rightPtr)
			quicksort(low, rightPtr);
		if (leftPtr < high)
			quicksort(leftPtr, high);
	}

	private static void exchange(int i, int j) {
		int temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}

	//---------------------------------
	
	static public void main(String[] args) {
		// int[] arr = {3,4,6,3,44,5,3,5,3,64,7,58,5,675,67,567};
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}

		sort(numbers);

		System.out.println("");
		
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
	}

}