
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HRRegexEmails {

	static int numberRows;
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		numberRows = sc.nextInt();

		List<String> list = new ArrayList<String>();
		Pattern gmailPattern = Pattern.compile(".+@gmail\\.com$");

		for (int i = 0; i < (numberRows); i++) {
			String tempName = sc.next();
			String tempEmail = sc.next();
			Matcher matcher = gmailPattern.matcher(tempEmail);
			if (matcher.find()) {
				list.add(tempName);
			}
		}
		
		Collections.sort(list);
		for(String name : list)
			System.out.println(name);
	}

}
