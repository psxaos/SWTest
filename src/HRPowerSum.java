import java.util.Scanner;

public class HRPowerSum {
	static int number, power;
	static int times = 0;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		number = sc.nextInt();
		power = sc.nextInt();
		int result = calculate(number, power, 1, 0);
		System.out.println(result);

	}

	static int calculate(int number, int power, int startNumber, int tempSuma) {
		int current = (int) Math.pow(startNumber, power);

		if (tempSuma + current == number) {
			return 1;
		} else if (tempSuma > number || current > number) {
			return 0;
		} else {
			return calculate(number, power, startNumber + 1, tempSuma + current)
					+ calculate(number, power, startNumber + 1, tempSuma);
		}

	}

}
