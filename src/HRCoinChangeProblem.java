//https://www.hackerrank.com/challenges/coin-change
public class HRCoinChangeProblem {
	static int kwota = 8;
	static int[] coins = { 1, 2, 5 };

	public static void main(String[] args) {
		System.out.println(max());
	}

	public static int getChangingWays() {
		int changingWays = 0;
		return changingWays;
	}

	public static long max() {
		long[] kwotaArr = new long[kwota + 1];
		long temp;
		kwotaArr[0] = 1; // przyjmujemy że kwotę zero
							// mozemy rozmienić na 1
							// sposobów
		int moneta;
		for (int indexMonety = 0; indexMonety < coins.length; indexMonety++) {
			moneta = coins[indexMonety];
			for (int j = moneta; j < kwotaArr.length; j++) {
				
				temp = kwotaArr[j - moneta];
				kwotaArr[j] = kwotaArr[j] + temp;
			}

		}
		return kwotaArr[kwota];
	}

}
