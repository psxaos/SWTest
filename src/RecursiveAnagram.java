import java.io.*;

public class RecursiveAnagram {
	static String word;
	static int size;
	static int count;
	static char[] arrChar = new char[100];

	public static void main(String[] args) throws IOException {
		System.out.println("inpit word: ");
		word = ReadString();
		size = word.length();
		count = 0;
		for (int j = 0; j < size; j++) {
			arrChar[j] = word.charAt(j);
		}
		doAnagram(size);
	}

	public static void doAnagram(int newSize) {
		if (newSize == 1) { // too small to rotate
			return;
		}
		for (int j = 0; j < newSize; j++) { // for each position
			doAnagram(newSize - 1);// anagram remaining letters
			if (newSize == 2) {// if innermost
				displayWord();
			}
			rotateWord(newSize);
		}

	}

	public static void rotateWord(int newSize) {
		int j;
		int position = size - newSize;
		char temp = arrChar[position]; // save first letter
		for (j = position + 1; j < size; j++) {// shift other letters
			arrChar[j - 1] = arrChar[j];
		}
		arrChar[j - 1] = temp;// put first on right

	}

	public static void displayWord() {
		if (count < 99) {
			System.out.print(" ");
		}
		if (count < 9) {
			System.out.print(" ");
		}
		System.out.print(++count + " ");
		for (int j = 0; j < size; j++) {
			System.out.print(arrChar[j]);
		}
		System.out.print("    ");
		System.out.flush();
		if (count % 6 == 0) {
			System.out.println("");
		}

	}

	public static String ReadString() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}

	public static int getInt(String s) {
		int i = Integer.parseInt(s);
		return i;
	}
}
