import jdk.nashorn.internal.runtime.regexp.joni.ScanEnvironment;

import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-29.
 */
public class HRSherlockAndArray {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int tests = sc.nextInt();

        for (int t = 0; t < tests; t++) {
            int size = sc.nextInt();
            int[] arr = new int[size];
            for (int i = 0; i < size; i++) {
                arr[i] = sc.nextInt();
            }

            System.out.println(solution(arr));
        }
        sc.close();
    }

    private static String solution(int[] arr) {
        int sumLeft = arr[0], sumRight = 0;
        if (arr.length == 1)
            return "YES";

        if (arr.length == 2 && (arr[0] == 0 || arr[1] == 0))
            return "YES";


        for (int i = 2; i < (arr.length); i++) {
            sumRight = sumRight + arr[i];
        }

        for (int i = 1; i < (arr.length - 1); i++) {
            if (sumLeft == sumRight)
                return "YES";
            sumLeft = sumLeft + arr[i];
            sumRight = sumRight - arr[i + 1];

        }

        return "NO";
    }


}
