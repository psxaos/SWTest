import java.io.*;

public class RecursiveTriangle {

	static int number;

	public static void main(String[] args) throws IOException {
		System.out.println("Enter number: ");
		number = getNumber();
		int answer = triangle(number);
		System.out.println("odpowied�: " + answer);
	}

	public static int triangle(int n) {
		System.out.println("entering= "+n);
		if (n == 1) {
			return 1;
		} else {
			int temp = n + triangle(n - 1);
			System.out.println("returning" + temp);
			return temp;
		}

	}

	public static int getNumber() throws IOException {
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		int n = Integer.parseInt(s);
		return n;
	}

}
