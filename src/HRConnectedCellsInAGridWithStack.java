import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-30.
 */
public class HRConnectedCellsInAGridWithStack {

    static Cell[][] arr;
    static Stack2 stack;
    static int result = 0, tempResult = 0;


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int noRows = sc.nextInt();
        int noCols = sc.nextInt();
        arr = new Cell[noRows + 2][noCols + 2];

        //border case
        for (int c = 0; c < noCols + 2; c++) {
            arr[0][c] = new Cell(0, c, 2);//visited = 2
            arr[noRows + 1][c] = new Cell(noRows + 1, c, 2);
        }
        for (int r = 0; r < noRows + 2; r++) {
            arr[r][0] = new Cell(r, 0, 2);//visited = 2
            arr[r][noCols + 1] = new Cell(r, noCols + 1, 2);
        }
        //filling the array
        int totalOfOnes = 0; //liczba jedynek do określenia wielkości stosu
        for (int r = 1; r <= noRows; r++) {
            for (int c = 1; c <= noCols; c++) {
                arr[r][c] = new Cell(r, c, sc.nextInt());
                if (arr[r][c].state == 1)
                    totalOfOnes++;
            }
        }
        sc.close();

        stack = new Stack2(totalOfOnes);

        //idziemy po tablicy szukając kolenej nieodwiedzonej wyspy
        for (int r = 1; r <= noRows; r++) {
            for (int c = 1; c <= noCols; c++) {

                if (arr[r][c].state == 1) { //gdy znaleźliśmy ląd (1)
                    stack.push(arr[r][c]);//na stos
                    arr[r][c].state++; //visited
                    tempResult = 1;
                    while (!stack.isEmpty()) {
                        Cell current = stack.pop();
                        dfs_visit(current);
                    }
                }
                if (tempResult > result)
                    result = tempResult;
            }
        }
        System.out.println(result);

        for (int r = 1; r <= noRows; r++) {
            for (int c = 1; c <= noCols; c++) {

                System.out.print(arr[r][c].state + " ");
            }
            System.out.println("");
        }


    }

    static void dfs_visit(Cell cell) {
        int r = cell.row;
        int c = cell.column;

        cell.state = 2; //mark as visited
        int dx[] = {-1, -1, -1, 0, 0, 1, 1, 1};
        int dy[] = {-1, 0, 1, -1, 1, -1, 0, 1};

        for (int i = 0; i < 8; i++) {
            int x = r + dx[i];
            int y = c + dy[i];
            if (arr[x][y].state == 1) {
                arr[x][y].state++; //current mark as visited
                stack.push(arr[x][y]);
                arr[x][y].state++;
                tempResult++;

            }
        }
    }
}

class Stack2 {
    Cell[] stackArray;
    int top;

    public Stack2(int n) {
        stackArray = new Cell[n];
        top = -1;
    }

    void push(Cell x) {
        stackArray[++top] = x;
    }

    Cell pop() {
        return stackArray[top--];
    }

    Cell peak() {
        return stackArray[top];
    }

    Boolean isEmpty() {
        return top == -1;
    }

}

class Cell {
    int row;
    int column;
    int state; //0=sea, 1=land, 2=visited

    public Cell(int row, int column, int state) {
        this.row = row;
        this.column = column;
        this.state = state;
    }
}



