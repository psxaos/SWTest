import java.util.Scanner;

public class HRQsort1Partition {
//�LE DZIA�A !!!! dane: {3,-7, 0} - wynik {0,-7, 3}!!!
	static int size;
	static int[] arr;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		size = sc.nextInt();
		arr = new int[size];
		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}

		sc.close();
		partition(arr[0], 0, size - 1);

		for (int i = 0; i < size; i++) {
			System.out.print(arr[i] + " ");
		}

	}

	static void swap(int ptrLeft, int ptrRight) {
		int temp;
		temp = arr[ptrLeft];
		arr[ptrLeft] = arr[ptrRight];
		arr[ptrRight] = temp;
	}

	static void partition(int pivot, int left, int right) {
		int leftPtr = left;
		int rightPtr = right;

		while (true) {
			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot && rightPtr > 0) {
				rightPtr--;
			}
			if (leftPtr < rightPtr) {
				swap(leftPtr, rightPtr);
			} else {
				break;
			}

		}
	}

}
