
public class Training {

}
//************ List **************
class LinkT{
	int weight;
	LinkT next;
	int nodeId;

	public LinkT(int nodeId, int weight){
		this.next=null;
		this.nodeId=nodeId;
		this.weight = weight;
	}
}

class LinkedListT{
	LinkT first;
	int size;
	int nLinks=0;
	
	public LinkedListT(){
		this.first=null;
		this.size=0;

	}
	boolean isEmpty(){
		return first==null;
	}	
	
	public void insert(int nodeId, int weight){
		LinkT newLink = new LinkT(nodeId, weight);
		newLink.next=first;
		first =newLink;
		nLinks++;
	}
	
	public LinkT removeFirst(){
		LinkT temp = first;
		first = first.next;
		nLinks--;
		return temp;
	}
	
	public LinkT find(int nodeId){
		if (first==null)
			return null;
		LinkT temp=first;
		while(temp.nodeId!=nodeId){
			if(temp.next==null){
				return null;
			}else{
				temp=temp.next;
			}
		}
		return temp;
	}
	public int size(){
		return nLinks;
	}
}
//*******Graph**********************
class GraphT{
	private LinkedListT[] arrayOfNodes;
	// budujemy graf jako tablic� list, w listach b�d� zawarte po��czenia
	//warto�� pola to id kolejnego w�z�a, waga to waga prz�s�a
	//ch�opacy robili tablic� wag oddzielnie
	//a list� zbudowali na tablicach
	
	public GraphT(int nNodes){
		arrayOfNodes = new LinkedListT[nNodes];
		//
		for (int i =0; i<nNodes;i++){
			arrayOfNodes[i]=new LinkedListT();
		}
	}
	
	
}