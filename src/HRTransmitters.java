import java.util.Scanner;

public class HRTransmitters {

	static int numberOfHouses = 0, range = 0, number = 0;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		numberOfHouses = sc.nextInt();// liczba domów
		range = sc.nextInt();// zasięg anteny
		int maxHousePosition = 0; // największy numer domu
		int[] mapOfHouses = new int[100000];
		int pointer = 0;// aktualna pozycja sprawdzana

		for (int i = 0; i < numberOfHouses; i++) {
			int temp = sc.nextInt();
			mapOfHouses[temp] = 1;

			if (maxHousePosition < temp)
				maxHousePosition = temp; // zapamiętujemy, największy nr domu
		}
		sc.close();

		while (pointer <= 100000) {

			// gdy osiągnęliśmy ostatni dom nie sprawdzamy dalej
			if (pointer > maxHousePosition) {
				break;
			}

			if (mapOfHouses[pointer] == 1 && pointer <= 100000) {
				// gdy trafiliśmy na dom
				// sprawdzamy gdzie postawić antenę aby ten dom był po prawej
				// granicy zasięgu przyszłej anteny
				for (int i = pointer + range; i >= pointer; i--) {
					if (mapOfHouses[i] == 1) {
						number++; // dodajemy antenę
						break;
					}

					// przesuwamy się do kolejnego pola poza zasięgiem tej
					// anteny
					pointer = i + range + 1;

				}

			} else {
				// nie ma tu domu idziemy dalej
				pointer++;
			}

		}

		System.out.println(number);
	}

}
