

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class FileOpen {

	public static void main(String[] args) {
		Scanner in = null;
		File file = new File("D:\\Eclipse Projects\\test");

		int testCases;
		int noOfBranches, nodeA, nodeB, time;
		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		testCases = in.nextInt();

		for (int t = 0; t < testCases; t++) {
			System.out.println("test case: " + t);
			noOfBranches = in.nextInt();
			System.out.println("numbr of branches : " + noOfBranches);

			for (int n = 0; n < (noOfBranches - 1); n++) {

				time = in.nextInt();
				nodeA = in.nextInt();
				nodeB = in.nextInt();
				System.out.println("time: " + time + " node a: " + nodeA + " node b: " + nodeB);
			}
		}
	}
}