import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-02.
 */
public class HRJourneyToTheMoon {

    static long result;
      static int noAstronauts;
    static boolean visited[];

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        noAstronauts = sc.nextInt();
        int noPairs = sc.nextInt();

        AstroGraph g = new AstroGraph(noAstronauts);


        for (int i = 0; i < noPairs; i++) {
            int firstId = sc.nextInt();
            int secondId = sc.nextInt();

            g.addUndirectedEdge(firstId, secondId); //adds astronaut to its pair

        }
        sc.close();

//        for (int i = 0; i < noAstronauts; i++) {
//            if (arr[i] != null) {
//                LinkAstro current = arr[i].first;
//                while (current != null) {
//
//                    System.out.println(i + " " + current.astronautId);
//                    current = current.next;
//                }
//            }
//        }
        result = ((long) (noAstronauts) * (noAstronauts - 1)) / 2; //total number of pairs of astronauts

        result = result - g.dfs();
        System.out.println(result);
    }


}

class LinkAstro {
    int astronautId;
    LinkAstro next;

    LinkAstro(int astronautId) {
        this.astronautId = astronautId;
        next = null;
    }
}

class LinkedListAstro {
    LinkAstro first = null;
    int size = 0;

    boolean isEmpty() {
        return first == null;
    }

    void insertFirst(int astronautId) {
        LinkAstro newLink = new LinkAstro(astronautId);
        newLink.next = first;
        this.first = newLink;
        size++;
    }

    int size() {
        return this.size;
    }

}

class AstroGraph {
    LinkedListAstro[] adj;
    boolean[] visited;

    AstroGraph(int size) {
        this.adj = new LinkedListAstro[size];
        for (int i = 0; i < size; i++) {
            adj[i] = new LinkedListAstro();
        }
    }

    void addUndirectedEdge(int firstID, int secondId) {
        adj[firstID].insertFirst(secondId);
        adj[secondId].insertFirst(firstID);
    }

    long dfs() {
        visited = new boolean[adj.length];
        long total = 0;
        for (int u = 0; u < adj.length; u++) {
            visited[u] = false;
        }
        for (int u = 0; u < adj.length; u++) {
            if (!visited[u]) {
                int cc = dfsVisit(u);
                total = total + ((long) cc * ((long)cc - 1)) / 2;
            }
        }
        return total;
    }

    int dfsVisit(int u) {
        visited[u] = true;
        int cc = 1;

        LinkAstro v = adj[u].first;
        while (v != null) {
            if (!visited[v.astronautId]) {
                cc = cc + dfsVisit(v.astronautId);
            }
            v = v.next;
        }
        return cc;
    }


}