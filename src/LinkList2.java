
public class LinkList2 {

	public Link2 first;
	public Link2 last;

	public LinkList2() {
		first = null;
		last = null;
	}

	public boolean isEmpty() {
		return (first == null);
	}

	public void insertFirst(double x) {
		Link2 newLink = new Link2(x);

		if (first == null) {
			last = newLink;
		} else {
			newLink.next = first;

		}
		first = newLink;
	}

	public void insertLast(double x) {
		Link2 newLink = new Link2(x);

		if (isEmpty()) {
			first = newLink;

		}
		last.next = newLink;
		last = newLink;
	}

	public Link2 removeFirst() {
		Link2 temp = first;
		first = first.next;
		return temp;
	}

	public void insertInOrder(double x) {
		Link2 newLink = new Link2(x);
		Link2 previous = null;
		Link2 current = first;

		while (current != null && current.data < x) {

			previous = current;
			current = current.next;

		}
		if (previous == null) {
			first = newLink;
			last = newLink;
		} else {
			previous.next = newLink;
		}
		newLink.next = current;
	}

	public void display() {
		Link2 link = first;
		while (link != null) {
			link.display();
			link = link.next;
		}
	}
}
