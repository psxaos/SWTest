import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-19.
 */
public class HRBFSShortestReach {
    static final int LENGTH = 6;
    static Scanner sc;


    public static void main(String[] args) {

        sc = new Scanner(System.in);
        int q = sc.nextInt();

        for (int i = 1; i <= q; i++) {

            int nodes = sc.nextInt();
            int edges = sc.nextInt();
            BFSGraph g = new BFSGraph(nodes);

            for (int j = 0; j < edges; j++) {//define a graph edges
                int u = sc.nextInt();
                int v = sc.nextInt();
                g.addUndirectedEdge(u - 1, v - 1); //input numbering starts from 1 so we sbstract 1 to start from 0

            }
            int start = sc.nextInt()-1;
            g.bfs(start);

            for (int n = 0; n < nodes; n++) { //print distance array
                if (n != start) { //ignore distance for start node
                    System.out.print(g.distanceArray[n] + " ");
                }
            }
            System.out.println("");
        }
        sc.close();
    }

}


class BFSGraph {
    int nodes;
    int[][] arr;
    BFSQueue q;
    int[] distanceArray;
    boolean[] visited;

    BFSGraph(int nodes) {
        this.nodes = nodes;
        this.arr = new int[nodes][nodes];
        this.distanceArray = new int[nodes];
        this.q = new BFSQueue(nodes);
        this.visited = new boolean[nodes];

        //initialize distance array to infinity (-1)
        for (int i = 0; i < nodes; i++) {
            distanceArray[i] = -1;
        }
    }

    void addUndirectedEdge(int u, int v) {
        this.arr[u][v] = HRBFSShortestReach.LENGTH;
        this.arr[v][u] = HRBFSShortestReach.LENGTH;
    }


    void bfs(int start) { //returns shortest reach from s

        BFSNode s = new BFSNode(start, 0);
        visited[s.id] = true;
        q.insert(s);

        while (!q.isEmpty()) {
            BFSNode u = q.peek();
            for (int i = 0; i < nodes; i++) { //for every neighbour

                if ((arr[u.id][i] == HRBFSShortestReach.LENGTH) && (visited[i] != true)) {//if edge u----v exists and v not visited
                    BFSNode v = new BFSNode(i, u.distance + 6);//distance +1
                    visited[v.id] = true; //mark visited
                    q.insert(v);//put into queue
                    distanceArray[v.id] = v.distance; //store distance to node v
                }
            }
        }

    }

    boolean isValid(BFSNode bfsNode) {
        return visited[bfsNode.id] == false;
    }


}

class BFSQueue {

    BFSNode[] qArray;
    int head = 0;
    int tail = -1;


    BFSQueue(int size) {
        qArray = new BFSNode[size];
    }

    void insert(BFSNode bfsNode) {
        qArray[++tail] = bfsNode;
    }

    BFSNode peek() {
        return qArray[head++];
    }

    boolean isEmpty() {
        return head > tail;
    }

}

class BFSNode {
    int id;
    int distance;

    BFSNode(int nodeId, int distance) {
        this.id = nodeId;
        this.distance = distance;
    }

}