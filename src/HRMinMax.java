import java.util.Arrays;
import java.util.Scanner;

public class HRMinMax {

	static int arr[];

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();
		arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		sc.close();
		// sort then iterate by k calculating difference and choose min one
		// quicksort(0, n - 1); //too slow on HR time constraints
		Arrays.sort(arr);
		int idx = 0;
		int diffValue = Integer.MAX_VALUE;

		while ((idx + k - 1) < n) {
			int temp = arr[idx + k - 1] - arr[idx];
			if (temp < diffValue) {
				diffValue = temp;
			}
			idx++;
		}
		System.out.println(diffValue);

	}

	private static void quicksort(int left, int right) {
		int leftPtr = left, rightPtr = right;
		int pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot) {
				rightPtr--;
			}

			if (leftPtr <= rightPtr) {
				swap(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}

			// recursion
			if (leftPtr < right) {
				quicksort(leftPtr, right);
			}
			if (rightPtr > left) {
				quicksort(left, rightPtr);
			}
		}

	}

	private static void swap(int leftPtr, int rightPtr) {
		int temp = arr[leftPtr];
		arr[leftPtr] = arr[rightPtr];
		arr[rightPtr] = temp;
	}

}
