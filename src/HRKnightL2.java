import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-24.
 */
public class HRKnightL2 {
    static State state;
    static int[][] visited;
    static State[] queue;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        sc.close();

        for (int i = 1; i < size; i++) {

            for (int j = 1; j < size; j++) {
                System.out.print(bfs(size, i, j) + " ");
            }
            System.out.println("");
        }
    }

    static int bfs(int size, int i, int j) {
        visited = new int[size][size];
        queue = new State[size * size];
        int head = 0;
        int tail = -1;

        int dx[] = {i, i, -i, -i, j, j, -j, -j};
        int dy[] = {j, -j, j, -j, i, -i, i, -i};

        queue[++tail] = new State(0, 0, 0);
        visited[0][0] = 1;

        while (head <= tail) {
            State previous = queue[head++];
            for (int a = 0; a < 8; a++) {
                State visiting = new State(previous.x + dx[a], previous.y + dy[a], previous.d + 1);
                if (isLegal(visiting, size) && (visited[visiting.x][visiting.y] != 1)) {
                    visited[visiting.x][visiting.y] = 1;
                    queue[++tail] = visiting;
                    if (visited[size - 1][size - 1] == 1) {
                        return visiting.d;
                    }

                }
            }

        }
        return -1;
    }

    static boolean isLegal(State state, int size) {
        return ((0 <= state.x) && (state.x < size) && (0 <= state.y) && (state.y < size));
    }

}

class State {
    public State(int x, int y, int d) {
        this.x = x;
        this.y = y;
        this.d = d;
    }

    int x, y, d;
}

