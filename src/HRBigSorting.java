import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;
import java.lang.String;

public class HRBigSorting {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		String[] unsorted = new String[n];
		for (int unsorted_i = 0; unsorted_i < n; unsorted_i++) {
			unsorted[unsorted_i] = in.next();
		}
		in.close();
		Arrays.sort(unsorted, new CompareStringNumbers());

		for (int i = 0; i < n; i++)
			System.out.println(unsorted[i]);
	}

	static class CompareStringNumbers implements Comparator<String> {

		@Override
		public int compare(String o1, String o2) {

			int o1Length = o1.length();
			int o2Length = o2.length();

			if (o1Length > o2Length) {
				return 1;
			} else if (o1Length < o2Length) {
				return -1;
			} else {
				return o1.compareToIgnoreCase(o2);
			}
		}
	}
}
