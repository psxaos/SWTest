
public class MaskGenerate {
	static int row, col;
	static Maska[] maskArray;
	static int maskArrayLastInsPtr = 0;
	static Maska m;

	public static void main(String[] args) {
		int maxMasks = 89;
		col = 10;
		maskArray = new Maska[maxMasks];
		// generate all possible masks
		// insert 00000 mask into mask Array on position 0
		m = new Maska(col);
		generate(m, 0);
		
		for(int i=0;i<89;i++){
			System.out.println(maskArray[i].toString());
		}
		
		
	}

	static void generate(Maska m, int index) {
		m = new Maska(m);
		if (index == 9) {
			addToMaskArray(m);
		} else {
			m.mask[index] = 1;
			if (index < 8) {
				m.mask[index + 1] = 0;
				generate(m, index + 2);
			} else {
				generate(m, index + 1);
			}

			m.mask[index] = 0;
			generate(m, index + 1);

		}

	}

	static void addToMaskArray(Maska m) {
		maskArray[maskArrayLastInsPtr] = m;
		maskArrayLastInsPtr++;
	}

}

class Maska {
	int size;
	int[] mask;
	int pointer = 0;

	Maska(int noOfWaferColumns) {
		size = noOfWaferColumns - 1;
		mask = new int[size];
	}

	Maska(Maska parent) {
		this(parent.size + 1);
		this.pointer = parent.pointer;
		for (int i = 0; i < parent.size; i++) {
			this.mask[i] = parent.mask[i];
		}
	}

	void addSubmask0() {
		mask[pointer] = 0;
		pointer++;
	}

	void addSubmask1() {
		mask[pointer] = 1;
		// mask[pointer + 1] = 0;
		pointer = pointer + 2;
	}

	boolean isFull() {
		if ((mask[size - 2] == 1 && pointer >= size - 2) || (mask[size - 2] == 0 && pointer > size - 1)) {
			return true;
		}
		return false;
	}
	
	public String toString(){
		String s ="";
		for (int i=0;i<size;i++){
			s=s+" "+mask[i];
		}
		
	return s;	
	}

}