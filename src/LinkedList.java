

public class LinkedList {

	private Link first;

	public LinkedList() {
		first = null;
	}

	public boolean isEmpty() {
		return (first == null);
	}

	public void insertFirst(int id, double data) {
		Link newLink = new Link(id, data);
		newLink.next = first;
		first = newLink;
	}

	public Link deleteFirst() {
		Link temp = first;
		first = first.next;
		return temp;
	}

	public void displayList() {
		System.out.println("List (first-->Last): ");
		Link current = first;
		while (current != null) {
			current.display();
			current = current.next;
		}
		System.out.println("");
	}

	public Link find(int id) {
		Link current = first;
		while (current.id != id) {
			if (current.next == null) {// not found
				return null;
			} else {
				current = current.next;
			}
		}
		return current; //  found
	}

	public Link delete(int id) {
		Link current = first;
		Link previous = first;

		while (current.id != id) {// until found
			if (current.next == null) {// end of list
				return null;
			} else {
				previous = current;// move forward current =next
				current = current.next;
			}
		}
		if (current == first) {// if found one is the first
			first = first.next;
		} else {
			previous.next = current.next; // bypass it

		}

		return current; // not found
	}

}
