

public class PriorityQueue {
	private int size;
	private long[] a;
	private int nItems;

	public PriorityQueue(int size) {
		nItems = 0;
		this.size = size;

		a = new long[size];

	}

	public void insert(long x) {
		int j;
		if (nItems == 0) {
			a[nItems++] = x;
			
		} else {
			for (j = nItems - 1; j >= 0; j--) { // start at end
				if (x > a[j]) {
					a[j + 1] = a[j];
				} else {
					break;
				}
			}
			a[j+1] = x;
			nItems++;
		}
	}

	public long remove() {
		return a[--nItems];// remove minimum item
	}

	public long peek() {
		return a[nItems - 1];
	}

	public boolean isEmpty() {
		return (nItems == 0);
	}

	public boolean isFull() {
		return (nItems == size);
	}

	public void display(){
		for (int i=0; i<nItems;i++){
		System.out.println(a[i]);
		}
	}

}
