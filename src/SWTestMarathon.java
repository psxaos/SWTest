import java.util.Scanner;

public class SWTestMarathon {

	static int totalEnergy; // 10 <= tE <=600
	static int totalDistance; // 5 <= tD <=40
	static int minutes; // 3 <= m <=6
	static int[] duration; // 0 <= s <=59
	static int[] reqEnergy; // 2 <= rE < 20

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		duration = new int[5];
		reqEnergy = new int[5];

		int t = sc.nextInt();

		for (int i = 0; i < t; i++) { // for every test case
			totalEnergy = sc.nextInt();
			totalDistance = sc.nextInt();

			// read min, sec, energy
			for (int j = 0; j < 5; j++) {
				int mins = sc.nextInt();
				int secs = sc.nextInt();
				duration[j] = convertMStoS(mins, secs);
				reqEnergy[j] = sc.nextInt();
			}

			int result = solution(totalEnergy, totalDistance, duration, reqEnergy);
			int[] solution = convertStoMS(result);
			System.out.println("#" + (i + 1) + " " + solution[0] + " " + solution[1]);
		}
		sc.close();
	}

	private static int solution(int totalEnergy, int totalDistance, int[] duration, int[] reqEnergy) {
		int result = Integer.MAX_VALUE;
		int tempEnergy = 0;
		int tempDistance = 0;
		int tempTime = 0;

		for (int a = 0; a <= totalDistance; a++) { // 0th pace
			if ((a == totalDistance) && (totalEnergy - a * reqEnergy[0]) >= 0) {
				tempTime = a * duration[0];
				if (tempTime < result) {
					result = tempTime;
				}
			}
			for (int b = a; b <= totalDistance; b++) { // 1st pace
				if ((b == totalDistance) && (totalEnergy - (a * reqEnergy[0] + (b - a) * reqEnergy[1])) >= 0) {
					tempTime = a * duration[0] + (b - a) * duration[1];
					if (tempTime < result) {
						result = tempTime;
					}
				}
				for (int c = b; c <= totalDistance; c++) { // 2nd pace
					if ((c == totalDistance) && (totalEnergy
							- (a * reqEnergy[0] + (b - a) * reqEnergy[1] + (c - b) * reqEnergy[2]) >= 0)) {
						tempTime = a * duration[0] + (b - a) * duration[1] + (c - b) * duration[2];
						if (tempTime < result) {
							result = tempTime;
						}
					}
					for (int d = c; d <= totalDistance; d++) {// 3rd pace
						if ((d == totalDistance) && (totalEnergy - (a * reqEnergy[0] + (b - a) * reqEnergy[1]
								+ (c - b) * reqEnergy[2] + (d - c) * reqEnergy[3]) >= 0)) {
							tempTime = a * duration[0] + (b - a) * duration[1] + (c - b) * duration[2]
									+ (d - c) * duration[3];
							if (tempTime < result) {
								result = tempTime;
							}
						}
						for (int e = d; e <= totalDistance; e++) {// 4th pace

							if ((e == totalDistance) && (totalEnergy - (a * reqEnergy[0] + (b - a) * reqEnergy[1]
									+ (c - b) * reqEnergy[2] + (d - c) * reqEnergy[3] + (e - d) * reqEnergy[4]) >= 0)) {
								tempTime = a * duration[0] + (b - a) * duration[1] + (c - b) * duration[2]
										+ (d - c) * duration[3] + (e - d) * duration[4];
								if (tempTime < result) {
									result = tempTime;
								}
							}
						}
					}
				}

			}

		}

		return result;

	}

	static int convertMStoS(int mins, int secs) {
		return (mins * 60 + secs);
	}

	static int[] convertStoMS(int seconds) {
		int[] time = new int[2]; // {m,s}
		time[0] = seconds / 60;
		time[1] = seconds % 60;

		return (time);
	}

}
