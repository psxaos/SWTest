import java.util.Scanner;

public class HRMinMAxSum {
	static long sum, max, min;
	static long temp;

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		sum = max = min = temp = in.nextLong();

		for (int i = 0; i < 4; i++) {
			temp = in.nextLong();

			sum += temp;
			if (temp < min){
				min = temp;

			}
				
			if (temp > max){
				max = temp;

			}
			
		}
		in.close();
		System.out.println((sum - max) + " " + (sum - min));
	}
}
