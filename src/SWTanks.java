import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-06-23.
 */
public class SWTanks {
    static Scanner sc = new Scanner(System.in);
    static int[][] map;
    static int visited[][][][] = new int[27][27][27][27];
    static int t; //no of test case, will be used also as a marker for visited vertices in one-time created visited array
    static int it;

    public static void main(String[] args) {
        //hint
        //not the tanks are the vertices of the graph but rather the state of the graph (positions of both tanks)

        t = sc.nextInt();

        for (it = 1; it <= t; it++) {
            int n = sc.nextInt();
            //first tank start/stop
            int s1r = sc.nextInt();
            int s1c = sc.nextInt();
            int g1r = sc.nextInt();
            int g1c = sc.nextInt();

            //second tank start/stop
            int s2r = sc.nextInt();
            int s2c = sc.nextInt();
            int g2r = sc.nextInt();
            int g2c = sc.nextInt();
            n = n + 2; //introducing border to the map array extending its size by 2 rows and cols


            //fill the map with obstacles, and border =1
            map = new int[n][n];

            //border with 1s
            for (int x = 0; x < n; x++) {
                map[0][x] = 1;
                map[n - 1][x] = 1;

                map[x][0] = 1;
                map[x][n - 1] = 1;
            }
            //obstacles from input
            for (int r = 1; r < n - 1; r++) {
                for (int c = 1; c < n - 1; c++) {
                    map[r][c] = sc.nextInt();
                }
            }
            //TODO output data in proper format
            System.out.println("# " + bfs(n, s1r, s1c, g1r, g1c, s2r, s2c, g2r, g2c));

        }

        sc.close();
    }

    static int bfs(int n, int s1r, int s1c, int g1r, int g1c, int s2r, int s2c, int g2r, int g2c) {

        TankState[] queue = new TankState[n * n * n * n];
        int head = 0;
        int tail = -1;

        int dx[] = {-1, -1, -1, 0, 0, 0, 1, 1, 1};
        int dy[] = {-1, 0, 1, -1, 0, 1, -1, 0, 1};

        queue[++tail] = new TankState(s1r, s1c, s2r, s2c, 0);
        visited[s2r][s1c][s2r][s2c] = it;

        if (visited[g1r][g1c][g2r][g2c] == it)  //if start == stop position in the begining
            return 0;

        while (head <= tail) {
            TankState u = queue[head++];
            for (int i = 0; i < dx.length; i++) {
                for (int j = 0; j < dy.length; j++) {

                    TankState v = new TankState(u.x1 + dx[i], u.y1 + dy[i], u.x2 + dx[j], u.y2 + dy[j], u.d + 1);
                    if (isValid(v)) {
                        //mark visited
                        visited[v.x1][v.y1][v.x2][v.y2] = it;
                        //push v
                        queue[++tail] = v;
                        if (visited[g1r][g1c][g2r][g2c] == it)
                            return v.d;

                    }
                }
            }
        }


        return -1;
    }

    //checks if the move is a valid one
    static boolean isValid(TankState ts) {
        //first tank not on obstacle & second tank not on obstacle & no tank in fire range of another one & not a visited state of the graph
        if (map[ts.x1][ts.y1] != 1 && map[ts.x2][ts.y2] != 1 && (absolute(ts.x1 - ts.x2) > 1 || absolute(ts.y1 - ts.y2) > 1) && visited[ts.x1][ts.y1][ts.x2][ts.y2] != it)
            return true;
        return false;
    }

    //helper method for calculating fire range area of the tank
    static int absolute(int x) {
        if (x < 0)
            return -x;
        return x;
    }
}

//snapshot of the state of the graph (both tank positions)
class TankState {
    int x1, y1, x2, y2, d; //d = moves counter (distance)

    TankState(int r1, int c1, int r2, int c2, int d) {
        this.x1 = r1;
        this.y1 = c1;
        this.x2 = r2;
        this.y2 = c2;
        this.d = d;
    }


}

