
public class MergeSort {

	public static void main(String[] args) {
		int[] arrayA = { 23, 47, 81, 95 };
		int[] arrayB = { 7, 14, 39, 55, 62, 74 };
		int[] arrayC = new int[(arrayA.length + arrayB.length)];

		merge(arrayA, arrayB, arrayC);
		display(arrayC);
	}

	public static void merge(int[] arrayA, int[] arrayB, int[] arrayC) {
		int lenA = arrayA.length;
		int lenB = arrayB.length;
		int indexA = 0, indexB = 0, indexC = 0;
		while (indexA < lenA && indexB < lenB) {// both not empty
			if (arrayA[indexA] < arrayB[indexB]) {
				arrayC[indexC++] = arrayA[indexA++];
			} else {
				arrayC[indexC++] = arrayB[indexB++];
			}
		}

		while (indexA < lenA) {// arrayA not empty
			arrayC[indexC++] = arrayA[indexA++];
		}

		while (indexB < indexB) {
			arrayC[indexC++] = arrayB[indexB++];
		}

	}

	public static void display(int[] array) {
		int l = array.length;
		for (int i = 0; i < l; i++) {
			System.out.print(array[i] + " ");

		}
		System.out.println("");
	}
}
