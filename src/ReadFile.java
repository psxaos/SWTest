

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFile {

	private String path;
	private int numberOfLines = 0;
//	private FileReader fr = null;
//	private BufferedReader br = null;

	public ReadFile(String path) {
		this.path = path;
	}

	public int numberOfLines() throws IOException {
		int numberOfLines = 0;
		String lineOfText = "";
				
		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);

		while ((lineOfText = br.readLine()) != null) {
			numberOfLines++;
		}
		br.close();
		return numberOfLines;
	}

	public String[] openFile() throws IOException {


		FileReader fr = new FileReader(path);
		BufferedReader br = new BufferedReader(fr);

		int numberOfLines = numberOfLines();
		String[] textFromFile = new String[numberOfLines];
		
		for (int i = 0; i < numberOfLines; i++) {
			textFromFile[i] = br.readLine();
		}
		br.close();
		return textFromFile;

	}
}
