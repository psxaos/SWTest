
//a class used in ListStack using LinkOne with only one data item
public class ListOne {
	private LinkOne first;
	private LinkOne last;

	public ListOne() {
		first = null;
		last = null;
	}

	public boolean isEmpty() {
		return (this.first == null);
	}

	public void insertFirst(double x) {
		LinkOne link = new LinkOne(x);
		if (isEmpty()) {
			last = link;
		}else{
			link.next = this.first;
		}
		this.first = link;
	}

	public void insertLast(double x) {
		LinkOne link = new LinkOne(x);
		if (isEmpty()) {
			this.first = link;
		} else {
			this.last.next = link;
		}
		this.last = link;
	}

	public void insertInOrder(double x) {
		LinkOne current = first;
		LinkOne previous = null;
		LinkOne temp = new LinkOne(x);

		while (current != null && x > current.data) {
			previous = current;
			current = current.next;
		}
		if (previous == null) {
			first = temp;
		} else {
			previous.next = temp;
		}
		temp.next = current;
	}

	public double deleteFirst() {
		LinkOne temp = this.first;
		this.first = this.first.next;
		return temp.data;
	}

	public void display() {
		LinkOne current = first;
		while (current != null) { // do until the end
			current.display(); // print itself
			current = current.next; // move forward
		}
		System.out.println("");
	}
}
