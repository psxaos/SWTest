

public class IntTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int a = 2;
		int b = 7;
		OrderedArray oa = new OrderedArray(10);
		System.out.println("size: " + oa.size());
		oa.insert(1);
		oa.insert(50);
		oa.insert(20);

		oa.display();
		System.out.println("size: " + oa.size());

		Stack st = new Stack(5);
		st.push(232L);
		st.push(333L);
		st.push(444);
		st.push(333L);
		st.push(444);
		System.out.println(st.pop());
		System.out.println("empty? " + st.isEmpty());
		st.display();
		// System.out.println((b)/2);

		Queue q = new Queue(5);
		q.insert(20L);
		q.insert(15L);
		q.insert(12L);
		System.out.println(q.remove());
		System.out.println(q.remove());
		q.insert(22L);
		q.insert(23L);
		q.insert(24L);
		System.out.println(q.remove());
		System.out.println(q.remove());
		System.out.println(q.remove());
		System.out.println(q.remove());

		PriorityQueue pq = new PriorityQueue(10);
		pq.insert(2);
		pq.insert(3);
		pq.insert(4);
		pq.insert(1);
		pq.insert(10);
		pq.insert(5);
		pq.display();
		
		System.out.println("LList");
		LinkedList ll=new LinkedList();
		ll.insertFirst(33, 333);
		ll.insertFirst(44, 444);
		ll.insertFirst(55, 555);
		ll.insertFirst(66, 666);
		System.out.println("find");
		Link l=ll.find(22);
		l.display();
		ll.deleteFirst();
		System.out.println("");
		ll.displayList();
		
		System.out.println("List Stack");
		ListStack lst = new ListStack();
		System.out.println("empty? " + lst.isEmpty());
		lst.push(232L);
		lst.push(333L);
		lst.push(444);
		lst.push(333L);
		lst.push(555L);
		lst.display();
		System.out.println(lst.pop());
		System.out.println("empty? " + lst.isEmpty());
		lst.display();
		
		ListOne lo = new ListOne();
		System.out.println("ListOne empty? " + lo.isEmpty());
		lo.insertInOrder(234);
		lo.insertInOrder(236);
		lo.display();
		
		LinkList2 lo2 = new LinkList2();
		System.out.println("LinkList2 empty? " + lo2.isEmpty());
		lo2.insertInOrder(234);
		lo2.insertInOrder(236);
		lo2.insertInOrder(233);
		lo2.insertInOrder(254);
		lo2.insertInOrder(244);
		lo2.insertInOrder(233);
		lo2.display();
		
		System.out.println("LList2");
		LinkList2 ll2=new LinkList2();
		ll2.insertFirst(333);
		ll2.insertFirst(444);
		ll2.insertFirst(555);
		ll2.insertFirst(666);
		System.out.println("xxxxx");
		ll2.display();
		ll2.removeFirst();
		System.out.println("");
		ll2.display();
	}

}
