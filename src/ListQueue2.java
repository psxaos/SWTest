
public class ListQueue2 {
	private LinkList2 list;

	public ListQueue2() {
		list = new LinkList2();
	}

	public void insert(double x){
		list.insertLast(x);
	}
	
	public double remove(){
		return (list.removeFirst().data);
		
	}
	
	public boolean isEmpty(){
		return (list.isEmpty());
	}
	
	public void display(){
		list.display();
	}
	
}
