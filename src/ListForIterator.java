
public class ListForIterator {

	private LinkForIterator first;
	private LinkForIterator last;

	public ListForIterator() {
		first = null;
		last = null;
	}

	public LinkForIterator getFirst() {
		return first;
	}

	public LinkForIterator getLast() {
		return last;
	}

	public boolean isEmpty() {
		return (first == null);
	}

	public void setFirst(LinkForIterator x) {
		first = x;
	}

	public ListIterator getListIterator() {
		return new ListIterator(this);
	}

	public void display() {
		LinkForIterator current = first;
		while (current != null) {
			current.display();
			current = current.next;
		}
	}

}
