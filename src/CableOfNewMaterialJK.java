import java.util.Scanner;

public class CableOfNewMaterialJK {
	// usuwamy ka�d� kraw�dz liczymy �rednic� dw�ch drzew z tgo powsta�ych
	// potem wybieramy dwa najd�u�sze wyliczone drzewa i ��czymy ich ko�ce
	// dodaj�c d�ugo�� usuni�tej kraw�dzi, kt�ra je po��czy
	// obliczanie �rednicy poddrzewa = bfs z dowolnego w�z�a, a potem bfs z
	// najdalszego w�z�a znalezionego w perwszym przeszukaniu

	static Scanner sc = new Scanner(System.in);
	static int t;
	static int result;

	public static void main(String[] args) {
		t = sc.nextInt(); // 4<=t<=2000 test cases

		// input data
		for (int it = 1; it <= t; it++) {
			int noDevices = sc.nextInt();
			// create graph
			CableGraph g = new CableGraph(noDevices);
			for (int dev = 0; dev < noDevices - 1; dev++) {
				int cost = sc.nextInt();
				int u = sc.nextInt() - 1;
				int v = sc.nextInt() - 1;
				g.addEdge(u, v, cost);
			}

			// for every u
			for (int u = 0; u < noDevices; u++) {
				CableList cl = g.edges[u];
				CableLink v = cl.first;
				while (v != null) {// disconnecting every v
					int d = g.diam(u, v);
					if (result < d) {
						result = d;
					}k
					v = v.next;
				}
			}

			System.out.println("#" + it + " " + result);
			// TODO: System.out.println("#" + it + " " + diam());
			result = 0; // before next test case reset result to 0
		}
		sc.close();
	}
}

class CableLink {
	CableLink next;
	int id;
	int cost;

	CableLink(int id, int cost) {
		this.id = id;
		this.next = null;
		this.cost = cost;
	}
}

class CableList {
	CableLink first = null;

	boolean isEmpty() {
		return first == null;
	}

	void add(int id, int cost) {
		CableLink temp = first;
		first = new CableLink(id, cost);
		first.next = temp;
	}
}

class CableGraph {
	CableList[] edges;
	// CableNode node;
	int[] distances; // distances to start node
	boolean[] visited;
	CableQueue q;
	int s; // bfs start node

	CableGraph(int size) {
		edges = new CableList[size];
		visited = new boolean[size];
		distances = new int[size];
		q = new CableQueue(size);

		for (int i = 0; i < size; i++) { // init edges list array
			edges[i] = new CableList();
		}
	}

	void addEdge(int u, int v, int distance) { // bidirectional edge
		edges[u].add(v, distance);
		edges[v].add(u, distance);
	}

	void resetBfsData() {
		for (int i = 0; i < visited.length; i++) {
			visited[i] = false;
			distances[i] = 0;
		}
	}

	void setDisconnectedNode(int nodeId) { // mark parent node as visited to
											// divide graph into two
		visited[nodeId] = true;
	}

	int bfs(int start, int disconnectedNode) { // disconnectedNode =-1 at start
		s = start;
		distances[s] = 0;
		q.resetQueue();
		resetBfsData();

		if (disconnectedNode != -1) { // mark visited to not to go through
										// disconnectedNode
			visited[disconnectedNode] = true;
		}
		q.add(s, 0);
		visited[s] = true;

		int farthest_node = start;
		while (!q.isEmpty()) {
			CableNode u = q.peek();
			// for every v...
			CableList vList = edges[u.nodeId];
			CableLink v = vList.first;
			while (v != null) { // pass through whole list of neighbours
				// if valid...
				if (!visited[v.id]) {
					distances[v.id] = u.distance + v.cost;
					q.add(v.id, distances[v.id]); // add to queue
					visited[v.id] = true; // mark visited
					if (distances[farthest_node] < distances[v.id])
						farthest_node = v.id;
				}
				v = v.next;
			}
		}

		return farthest_node;
	}

	int diam(int start, CableLink edge) {
		int bfs1EndPoint = bfs(start, edge.id);
		bfs1EndPoint = bfs(bfs1EndPoint, edge.id);
		int diam1 = distances[bfs1EndPoint];

		int bfs2EndPoint = bfs(edge.id, start);
		bfs2EndPoint = bfs(bfs2EndPoint, start); // whole 2nd graph
		int diam2 = distances[bfs2EndPoint];

		return (diam1 + diam2 + edge.cost);
	}
}

class CableNode {
	int nodeId;
	int distance;

	CableNode(int nodeId, int distance) {
		this.nodeId = nodeId;
		this.distance = distance;
	}
}

class CableQueue {
	CableNode[] qArray;
	int head;
	int tail;
	int size;

	CableQueue(int size) {
		this.size = size;
		qArray = new CableNode[size];
		resetQueue();
	}

	boolean isEmpty() {
		return (head > tail);
	}

	void add(int uid, int distance) {
		qArray[++tail] = new CableNode(uid, distance);
	}

	CableNode peek() {
		return qArray[head++];
	}

	void resetQueue() {
		head = 0;
		tail = -1;
	}
}