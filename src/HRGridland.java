import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-19.
 */
public class HRGridland {

    static Line[] listOfLines;
    static long citySize;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int noRows = sc.nextInt();
        int noCols = sc.nextInt();
        citySize = (long) noRows * (long) noCols;
        int noOfLines = sc.nextInt();

        if (noOfLines == 0) {
            System.out.println("" + citySize);
            return;
        }
        listOfLines = new Line[noOfLines];

        for (int k = 0; k < noOfLines; k++) {
            listOfLines[k] = new Line(sc.nextInt(), sc.nextInt(), sc.nextInt());
        }
        sc.close();

        //sortujemy linie
        Arrays.sort(listOfLines);

        Line currentLine;
        Line prevLine = new Line(-1, -1, -1);

        for (int i = 0; i < noOfLines; i++) {

            currentLine = listOfLines[i]; //bierzemy kolejną linię

            if (prevLine.row == currentLine.row) {//są w tym samym wierszu, zliczamy długość w wierszu i odejmujemy od rozmiaru miasta

                if (currentLine.start > prevLine.end) {//rozłączne
                    citySize = citySize - length(currentLine.start, currentLine.end);
                    prevLine = currentLine;
                    continue;
                } else {//nachodzą na siebie
                    if (currentLine.end > prevLine.end) {//wystaje poza poprzedni
                        citySize = citySize - length(prevLine.end+1, currentLine.end);
                        prevLine = currentLine;

                        continue;
                    } else {//zawiera się w poprzednim
                        continue;
                    }
                }
            } else { //ten jest w kolejnym wierszu
                citySize = citySize - length(currentLine.start, currentLine.end);
                prevLine = currentLine;
                continue;
            }

        }

        System.out.println(citySize);
    }

    static int length(int start, int end) {
        return end - start + 1;
    }

}

class Line implements Comparable<Line> {
    int row;
    int start;
    int end;

    Line(int row, int start, int end) {
        this.row = row;
        this.start = start;
        this.end = end;
    }

    public String toString() {
        return "row" + " " + "start" + " " + "stop";
    }

    @Override
    public int compareTo(Line l) {
        //citySize<10^9 so we just cast to integer (max int is 2147483647 > 10^9 so no overflow)
        int result = this.row - l.row;
        if (result != 0)
            return result;

        result = this.start - l.start;
        if (result != 0)
            return result;

        result = this.end - l.end;
        if (this.start != 0)
            return result;

        return result;
    }
}