import java.util.Scanner;

public class HRPrimeNotPrime {
	static int nTests;
	static Scanner in = new Scanner(System.in);
	static int current = 1;

	public static void main(String[] args) {

		nTests = in.nextInt();

		for (int i = 0; i < nTests; i++) {
			current = in.nextInt();
			displayAnswer(checkPrime(current));
		}
	}

	static boolean checkPrime(int input) {

		if (current == 1) {
			return false;

		}
		if (current == 2 || current == 3) {
			return true;
		}

		for (int i = 2; i * i <= input; i++) {
			if (input % i == 0) {
				return false;
			}
		}
		return true;
	}

	static void displayAnswer(boolean result) {
		if (result == true) {
			System.out.println("Prime");
		} else {
			System.out.println("Not prime");
		}

	}

}
