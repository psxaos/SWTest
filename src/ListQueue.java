
public class ListQueue {
	ListOne list = null;

	public ListQueue() {
		list = new ListOne();

	}

	public void insert(double x) {
		list.insertFirst(x);
	}

	public double removeFirst() {
		return list.deleteFirst();
	}

	public boolean isEmpty() {
		return (list.isEmpty());
	}

	public void display() {
		list.display();
	}

}
