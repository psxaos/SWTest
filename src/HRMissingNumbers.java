import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-18.
 */
public class HRMissingNumbers {
    static long firstSize, secondSize;


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        firstSize = sc.nextLong(); //ilość liczb w mniejszym zbiorze

        int[] arr = new int[202];
        int[] results = new int[202];
        int delta = 0;//różnica między wartością liczby a środkowym adresem tablicy 100

        //czytamy pierwszą liczbę z mniejszego zbioru, obliczamy deltę i wstawiamy w srodek tablicy 100
        int first = sc.nextInt();

        delta = first - 100;
        //pierwsza odczytana liczba ma index 100 - odzyskamy ja dodająć deltę, indeksy innych liczb obliczaamy użyciem tej samej delty
        arr[100]++;

        //dodajemy pierwsze liczby do tablicy (ilości wystąpień) (pierwszą już wczytaliśmy na pozycji 100)
        for (long i = 1; i < firstSize; i++) {
            int temp = sc.nextInt();
            temp = temp - delta;
            arr[temp]++;
        }

        secondSize = sc.nextLong(); //ilość liczb w większym zbiorze
        //odejmujemy wystąpienia liczb z drugiej tablicy
        for (long i = 0; i < secondSize; i++) {
            int temp = sc.nextInt();
            temp = temp - delta;
            arr[temp]--;
        }
        sc.close();

        //teraz wybieramy z tablicy te indexy liczb które !=0 i zapisujemy je już jako konkretne liczby w nowej tablicy
        for (int i = 0; i < 202; i++) {
            if (arr[i] != 0) {
                int temp = i;
                temp = temp + delta;
                results[i] = temp;
            }
        }

        //sortujemy tablicę
        Arrays.sort(results);

        //drukujemy wyniki

        for (int i = 0; i < results.length; i++)
            if (results[i] != 0) {
                System.out.print(results[i] + " ");
            }
    }
}
