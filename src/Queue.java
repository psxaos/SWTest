

public class Queue {
    private long[] q;
    private int front;
    private int rear;
    private int size;
    private int nItems;

    // constructor
    public Queue(int size) {
        this.size = size;
        q = new long[size];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void insert(long x) {
        if (rear == size - 1)  //wrap around
            rear = -1;
        q[++rear] = x;
        nItems++;
    }

    public long remove() {
        long temp = q[front++];
        if (front == size)
            front = 0;
        nItems--;
        return temp;
    }

    public long peek() {
        return q[front];
    }


    public boolean isFull() {
        return (nItems == size);
    }

    public boolean isEmpty() {
        return (nItems == 0);
    }

}
