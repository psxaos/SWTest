import java.util.Scanner;

public class HRGreedyFlorist {

	static int[] arr;

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		int numFlowers = sc.nextInt();
		int noFriends = sc.nextInt();
		arr=new int[numFlowers];
		
		for (int i = 0; i < numFlowers; i++) {
			arr[i] = sc.nextInt();
		}
		// dort descending
		qSortReverse(0, arr.length - 1);

		// traverse flower array buying all flowers by all friends in a row
		int boughtFlowers = 0;
		int buyingFriend = 0;
		int cycle = 1; //no of cycle thorough all friends
		int price = 0;
		while (boughtFlowers < numFlowers) {
			if (buyingFriend > noFriends - 1) {
				cycle++;
				buyingFriend = 0;
			}
			
			buyingFriend++;
			price = price + (cycle)*arr[boughtFlowers];
			boughtFlowers++;
		}
		System.out.println(price);
	}

	private static void qSortReverse(int left, int right) {

		int leftPtr = left, rightPtr = right;
		int pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {
			while (arr[leftPtr] > pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] < pivot) {
				rightPtr--;
			}
			if (rightPtr >= leftPtr) {
				swap(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}

			// recursion
			if (left < rightPtr) {
				qSortReverse(left, rightPtr);
			}
			if (right > leftPtr) {
				qSortReverse(leftPtr, right);
			}
		}
	}

	private static void swap(int leftPtr, int rightPtr) {
		int temp = arr[leftPtr];
		arr[leftPtr] = arr[rightPtr];
		arr[rightPtr] = temp;
	}

}
