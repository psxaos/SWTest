
//a class to use in ListStack as a Link with only one data item 
public class LinkOne {

	double data;
	LinkOne next;

	public LinkOne(double data) {
		this.data = data;

	}
	public void display(){
		System.out.print(data+" | ");
	}
}
