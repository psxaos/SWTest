

public class ListStack {
	private ListOne list;

	public ListStack() {
		list = new ListOne();
	}
	
	public void push(double x){
		list.insertFirst(x);
	}
	
	public double pop(){
		return list.deleteFirst();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
	
	public void display(){
		list.display();
	}
	
}
