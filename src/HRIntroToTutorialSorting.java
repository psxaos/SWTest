import java.util.Scanner;

public class HRIntroToTutorialSorting {
	static int value, size;
	static int[] arr;

	public static void main(String[] args) {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		Scanner sc = new Scanner(System.in);
		value = sc.nextInt();
		size = sc.nextInt();
		arr = new int[size];
		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}
		sc.close();

		System.out.println(binarySearch(arr, value));
	}

	private static int binarySearch(int[] array, int value) {

		int left = 0, right = array.length - 1;
		while (left <= right) {
			int middle = (right + left) / 2;
			if (value < array[middle]) {
				right = middle - 1;
			} else if (value > array[middle]) {
				left = middle + 1;
			} else {
				return middle;
			}

		}
		return -1;
	}

}
