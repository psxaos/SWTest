import java.util.Scanner;

public class HRMarkAndToys {
	static int[] arr;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int n = sc.nextInt();
		int k = sc.nextInt();
		arr = new int[n];
		for (int i = 0; i < n; i++) {
			arr[i] = sc.nextInt();
		}
		sc.close();

		quicksort(0, arr.length - 1);
		int result = 0;
		int idx = 0;
		while (k > 0 && idx < n) {
			if (arr[idx] <= k) {
				result++;
				k = k - arr[idx];
			}
			idx++;
		}
		System.out.println(result);
	}

	static void quicksort(int left, int right) {
		int leftPtr = left;
		int rightPtr = right;
		int pivot = arr[left + (right - left) / 2];

		while (leftPtr <= rightPtr) {

			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot) {
				rightPtr--;
			}

			if (leftPtr <= rightPtr) {
				swap(leftPtr, rightPtr);
				leftPtr++;
				rightPtr--;
			}
		}

		// recursion
		if (left < rightPtr) {
			quicksort(left, rightPtr);
		}

		if (right > leftPtr) {
			quicksort(leftPtr, right);
		}

	}

	static void swap(int a, int b) {
		int temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}
}
