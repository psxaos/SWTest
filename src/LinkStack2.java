
public class LinkStack2 {
private LinkList2 list;
	
	
	public LinkStack2(){
		list = new LinkList2();
	}
	
	public void push(double x){
		list.insertFirst(x);
	}
	
	public double pop(){
		return (list.removeFirst().data);
	}
	
	public void display(){
		list.display();
	}
	
	public boolean isEmpty(){
		return list.isEmpty();
	}
}
