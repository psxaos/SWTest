import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-15.
 */
public class HRKnightL {

    static int size;
    static Place[][] chessBoard;
    static ChessQueue queue;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        size = sc.nextInt();
        sc.close();


        //dla każdego przypadku (i,j)
        for (int i = 1; i < size; i++) {
            int result = -1;

            for (int j = 1; j < size; j++) {
                System.out.print(solution(size, i, j));
                System.out.print(" ");

            }
            //kolejny wiersz
            System.out.println("");
        }
    }

    static int solution(int size, int i, int j) {
        queue = new ChessQueue(size * size);
        chessBoard = new Place[size][size];
        int x = 0, y = 0, distance = 0, tempX = 0, tempY = 0, visited = 0;
        Place current;
        chessBoard[0][0] = new Place(x, y, distance, visited); //init
        queue.put(new Place(x, y, distance, visited));

        while (!queue.isEmpty()) {
            current = queue.get();
            x = current.x;
            y = current.y;
            distance = current.distance;
            visited = current.visited;
            int result = 0;
            //znajdujemy nexty
            if (chessBoard[x][y].visited == 0) {//jeszcze tu nie byłem

//1. x+i y+j
                tempX = x + i;
                tempY = y + j;
                result = processPlace(tempX, tempY, distance);
                if (result!=0)
                    return result;

//2. x-i y+j
                tempX = x - i;
                tempY = y + j;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//3. x-i y-j
                tempX = x - i;
                tempY = y - j;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//4. x+i y-j
                tempX = x + i;
                tempY = y - j;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//5. x+j y+i
                tempX = x + j;
                tempY = y + i;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//6. x+j y-i
                tempX = x + j;
                tempY = y - i;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//7. x-j y-i
                tempX = x - j;
                tempY = y - i;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
//8. x-j y+i
                tempX = x - j;
                tempY = y + i;
                if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
                    if ((tempX == size - 1) && (tempY == size - 1)) {
                        return distance + 1;
                    }
                    if (chessBoard[tempX][tempY] == null) {
                        chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                        queue.put(chessBoard[tempX][tempY]);
                    }
                }
                chessBoard[x][y].visited = 1; //odwiedzony
            }
//            if (queue.isFull() == true)
//                return -1;
        }
        return -1;
    }

    static int processPlace(int tempX, int tempY, int distance) {
        if (tempX >= 0 && tempY >= 0 && tempX < size && tempY < size) {
            if ((tempX == size - 1) && (tempY == size - 1)) {
                return distance + 1;
            }
            if (chessBoard[tempX][tempY] == null) {
                chessBoard[tempX][tempY] = new Place(tempX, tempY, distance + 1, 0);
                queue.put(chessBoard[tempX][tempY]);
            }

        }
        return 0;
    }
}


class ChessQueue {
    int head = 0, tail = -1, nItems = 0;
    Place[] queueArray;
    int size;

    ChessQueue(int size) {
        this.size = size;
        queueArray = new Place[size];
    }

    void put(Place place) {
        if (tail == size - 1)//wrap around
            tail = -1;
        queueArray[++tail] = place;
        nItems++;
    }

    Place get() {
        Place temp = queueArray[head++];
        if (head == size)
            head = 0;
        nItems--;
        return temp;
    }

    boolean isEmpty() {
        return (nItems == 0);
    }

    boolean isFull() {
        return (nItems == size);
    }
}

class Place {
    int x = 0, y = 0, distance = 0, visited = 0;

    public Place(int x, int y, int distance, int visited) {
        this.x = x;
        this.y = y;
        this.distance = distance;
        this.visited = visited;
    }

}