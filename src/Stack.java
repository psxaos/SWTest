

public class Stack {
	private int size;
	private long[] stackArray;
	private int top;

	// ----constructor-----------
	public Stack(int size) {
		this.size = size;
		stackArray = new long[size];
		top = -1;
	}
	// --------------------------

	public void push(long x) {
		stackArray[++top] = x;
	}

	public long pop() {
		return stackArray[top--];
	}

	public long peek(){
		return stackArray[top];
	}
	
	public boolean isEmpty(){
		return (top==-1);
	}
	
	public boolean isFull(){
		return (top==size-1);
	}
	
	public void display(){
		for (int i =0; i<=top;i++){
			System.out.print(stackArray[i]+" ");
			System.out.println(" ");
		}
	}
}
