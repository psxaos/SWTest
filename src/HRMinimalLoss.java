import java.util.Scanner;

/**
 * Created by p.sobczynski on 2017-05-24.
 */
public class HRMinimalLoss {
    static Price arr[];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int number = sc.nextInt();

        long minimalLoss = Long.MIN_VALUE;

        arr = new Price[number];

        //input data
        for (int i = 0; i < number; i++) {
            arr[i] = new Price(sc.nextLong(), i);
        }
        sc.close();
        //sort
        quicksort(0, number - 1);

        //scan linearly find minimal loss
        for (int i = 1; i < number; i++) {
            long temp = (arr[i - 1].price - arr[i].price);
            if (temp > minimalLoss && temp < 0 && arr[i - 1].year > arr[i].year) {
                minimalLoss = temp;
            }
        }
        System.out.println("" + minimalLoss * (-1));
    }


    static void swap(int i, int j) {
        Price temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;

    }

    static void quicksort(int low, int high) {
        int i = low;
        int j = high;

        Price pivot = arr[low + (high - low) / 2];

        while (i <= j) {
            while (arr[i].price < pivot.price) {
                i++;
            }
            while (arr[j].price > pivot.price) {
                j--;
            }
            if (i <= j) {
                swap(i, j);
                i++;
                j--;
            }
        }
        //Recursion
        if (low < j)
            quicksort(low, j);
        if (i < high)
            quicksort(i, high);

    }


}

class Price {
    long price;
    int year;

    public Price(long price, int year) {
        this.price = price;
        this.year = year;
    }
}