﻿DFS:
===============================================================
   function VisitNode(u):
       oznacz u jako odwiedzony
       dla każdego wierzchołka v na liście sąsiedztwa u:
           jeżeli v nieodwiedzony:
               VisitNode(v)
   function DepthFirstSearch(Graf G):
       dla każdego wierzchołka u z grafu G:
           oznacz u jako nieodwiedzony
       dla każdego wierzchołka u z grafu G:
           jeżeli u nieodwiedzony:
               VisitNode(u) 

BFS:
===============================================================
funkcja BreadthFirstSearch (Graf G, Wierzchołek s)
    dla każdego wierzchołka u z G:
        kolor[u] = biały
        odleglosc[u] = inf
        rodzic[u] = NIL
    kolor[s] = SZARY
    odleglosc[s] = 0
    rodzic[s] = NIL
    Q.push(s)
    dopóki kolejka Q nie jest pusta:
        u = Q.pop()
        dla każdego v z listy sąsiedztwa u:
            jeżeli v jest biały:
                kolor[v] = SZARY
                odleglosc[v] = odleglosc[u] + 1
                rodzic[v] = u
                Q.push(v)
        kolor[u] = CZARNY

		
		
		
		
		
QuickSort:
===============================================================	
		
public class HRQsort1Partition {

	static int size;
	static int[] arr;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		size = sc.nextInt();
		arr = new int[size];
		for (int i = 0; i < size; i++) {
			arr[i] = sc.nextInt();
		}

		sc.close();
		partition(arr[0], 0, size - 1);

		for (int i = 0; i < size; i++) {
			System.out.print(arr[i] + " ");
		}

	}

	static void swap(int ptrLeft, int ptrRight) {
		int temp;
		temp = arr[ptrLeft];
		arr[ptrLeft] = arr[ptrRight];
		arr[ptrRight] = temp;
	}

	static void partition(int pivot, int left, int right) {
		int leftPtr = left;
		int rightPtr = right;

		while (true) {
			while (arr[leftPtr] < pivot) {
				leftPtr++;
			}
			while (arr[rightPtr] > pivot && rightPtr > 0) {
				rightPtr--;
			}
			if (leftPtr < rightPtr) {
				swap(leftPtr, rightPtr);
			} else {
				break;
			}

		}
	}

}		
		

QuickSort Vogella:
===============================================================
public class QuickSortVogella {

	private static int[] numbers = { 1,3,9,8,2,7,5 };
	static private int number;

	static public void main(String[] args) {
		// int[] arr = {3,4,6,3,44,5,3,5,3,64,7,58,5,675,67,567};
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}

		sort(numbers);

		System.out.println("");
		
		for (int i = 0; i < numbers.length; i++) {
			System.out.print(numbers[i] + " ");
		}
	}
	//---------------------------

	public static void sort(int[] values) {
		if (values == null || values.length == 0) {
			return;
		}
		numbers = values;
		number = values.length;
		quicksort(0, number - 1);
	}

	private static void quicksort(int low, int high) {
		int i = low, j = high;

		int pivot = numbers[low + (high - low) / 2];

		while (i <= j) {

			while (numbers[i] < pivot) {
				i++;
			}
			while (numbers[j] > pivot) {
				j--;
			}
			if (i <= j) {
				exchange(i, j);
				i++;
				j--;
			}
		}
		// recursion
		if (low < j)
			quicksort(low, j);
		if (i < high)
			quicksort(i, high);
	}

	private static void exchange(int i, int j) {
		int temp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = temp;
	}

}

Queue
===============================================================

public class Queue {
    private long[] q;
    private int front;
    private int rear;
    private int size;
    private int nItems;

    // constructor
    public Queue(int size) {
        this.size = size;
        q = new long[size];
        front = 0;
        rear = -1;
        nItems = 0;
    }

    public void insert(long x) {
        if (rear == size - 1)  //wrap around
            rear = -1;
        q[++rear] = x;
        nItems++;
    }

    public long remove() {
        long temp = q[front++];
        if (front == size)
            front = 0;
        nItems--;
        return temp;
    }

    public long peek() {
        return q[front];
    }


    public boolean isFull() {
        return (nItems == size);
    }

    public boolean isEmpty() {
        return (nItems == 0);
    }

}


Stack
===============================================================


public class Stack {
	private int size;
	private long[] stackArray;
	private int top;

	// ----constructor-----------
	public Stack(int size) {
		this.size = size;
		stackArray = new long[size];
		top = -1;
	}
	// --------------------------

	public void push(long x) {
		stackArray[++top] = x;
	}

	public long pop() {
		return stackArray[top--];
	}

	public long peek(){
		return stackArray[top];
	}
	
	public boolean isEmpty(){
		return (top==-1);
	}
	
	public boolean isFull(){
		return (top==size-1);
	}
	
	public void display(){
		for (int i =0; i<=top;i++){
			System.out.print(stackArray[i]+" ");
			System.out.println(" ");
		}
	}
}



LinkedList
===============================================================

class Link {
	public int id;
	public double data;
	public Link next;

	public Link(int id, double data) {
		this.id = id;
		this.data = data;
	}

	public void display() {
		System.out.print("id: " + id + ", data: " + data);
	}
}


public class LinkedList {

	private Link first;

	public LinkedList() {
		first = null;
	}

	public boolean isEmpty() {
		return (first == null);
	}

	public void insertFirst(int id, double data) {
		Link newLink = new Link(id, data);
		newLink.next = first;
		first = newLink;
	}

	public Link deleteFirst() {
		Link temp = first;
		first = first.next;
		return temp;
	}

	public void displayList() {
		System.out.println("List (first-->Last): ");
		Link current = first;
		while (current != null) {
			current.display();
			current = current.next;
		}
		System.out.println("");
	}

	public Link find(int id) {
		Link current = first;
		while (current.id != id) {
			if (current.next == null) {// not found
				return null;
			} else {
				current = current.next;
			}
		}
		return current; //  found
	}

	public Link delete(int id) {
		Link current = first;
		Link previous = first;

		while (current.id != id) {// until found
			if (current.next == null) {// end of list
				return null;
			} else {
				previous = current;// move forward current =next
				current = current.next;
			}
		}
		if (current == first) {// if found one is the first
			first = first.next;
		} else {
			previous.next = current.next; // bypass it

		}

		return current; // not found
	}

}


Priority Queue
===============================================================


public class PriorityQueue {
	private int size;
	private long[] a;
	private int nItems;

	public PriorityQueue(int size) {
		nItems = 0;
		this.size = size;

		a = new long[size];

	}

	public void insert(long x) {
		int j;
		if (nItems == 0) {
			a[nItems++] = x;
			
		} else {
			for (j = nItems - 1; j >= 0; j--) { // start at end
				if (x > a[j]) {
					a[j + 1] = a[j];
				} else {
					break;
				}
			}
			a[j+1] = x;
			nItems++;
		}
	}

	public long remove() {
		return a[--nItems];// remove minimum item
	}

	public long peek() {
		return a[nItems - 1];
	}

	public boolean isEmpty() {
		return (nItems == 0);
	}

	public boolean isFull() {
		return (nItems == size);
	}

	public void display(){
		for (int i=0; i<nItems;i++){
		System.out.println(a[i]);
		}
	}

}



Dijkstra - shortest path
===============================================================

Dijkstra(G,w,s):
   dla każdego wierzchołka v w V[G] wykonaj
      d[v] := nieskończoność
      poprzednik[v] := niezdefiniowane
   d[s] := 0
   Q := V
   dopóki Q niepuste wykonaj
      u := Zdejmij_Min(Q)
      dla każdego wierzchołka v – sąsiada u wykonaj
         jeżeli d[v] > d[u] + w(u, v) to
            d[v] := d[u] + w(u, v)
            poprzednik[v] := u
            Dodaj(Q, v)

   Wyświetl("Droga wynosi: " + d[v])
   
Binary Search
============================================================

class BinarySearch {
	public static int binarySearch(int[] a, int key) {

		int lo = 0;
		int hi = a.length - 1;

		while (lo <= hi) {
			int mid = (lo + hi) / 2;
			if (key < a[mid])
				hi = mid - 1;
			else if (key > a[mid])
				lo = mid + 1;
			else
				return mid;
		}
		return -1;

	}
}

Largest rectangle in histogram
===========================
public int largestRectangleArea(int[] height) {
	if (height == null || height.length == 0) {
		return 0;
	}
 
	Stack<Integer> stack = new Stack<Integer>();
 
	int max = 0;
	int i = 0;
 
	while (i < height.length) {
		//push index to stack when the current height is larger than the previous one
		if (stack.isEmpty() || height[i] >= height[stack.peek()]) {
			stack.push(i);
			i++;
		} else {
		//calculate max value when the current height is less than the previous one
			int p = stack.pop();
			int h = height[p];
			int w = stack.isEmpty() ? i : i - stack.peek() - 1;
			max = Math.max(h * w, max);
		}
 
	}
 
	while (!stack.isEmpty()) {
		int p = stack.pop();
		int h = height[p];
		int w = stack.isEmpty() ? i : i - stack.peek() - 1;
		max = Math.max(h * w, max);
	}
 
	return max;
}